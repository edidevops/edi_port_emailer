/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @author h.patel
 */
package EmailAlert.lib;

import com.edi.common.Logging;
import com.edi.common.mail4EDI.SendFile;
import com.edi.gmailer;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CommonMethods {

    private Logging log = new Logging("Automation");    
    db db_com = new db();
    private String LogFilePath = "";
    private int bAutoRun = 0;

    public CommonMethods(String Log, int Auto) {
        LogFilePath = Log;
        bAutoRun = Auto;
    }

    public void writeHTML(String sfile, String svalue) {
        try {
            if (sfile != null && svalue != null) {
                BufferedWriter out = null;
                File logFile = new File(sfile);
                if (logFile.exists()) {
                    out = new BufferedWriter(new FileWriter(sfile, true));
                } else {
                    out = new BufferedWriter(new FileWriter(sfile, false));
                }
                svalue = svalue.replace("null", "");
                out.write(svalue);
                out.close();
                logFile = null;

            }
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "writeHTML: " + e.getMessage());
            } else {
                log.append("writeHTML: " + e.getMessage(), LogFilePath, "fdp_port_emailer");
            }
        }
    }
    
    public void writeFile(String sfile, String svalue) {
        try {
            if (sfile != null && svalue != null) {
                BufferedWriter out = null;
                File logFile = new File(sfile);
                if (logFile.exists()) {
                    out = new BufferedWriter(new FileWriter(sfile, true));
                } else {
                    out = new BufferedWriter(new FileWriter(sfile, false));
                }
                svalue = svalue.replace("null", "");
                out.write(svalue + "\r\n");
                out.close();
                logFile = null;

            }
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "writeHTML: " + e.getMessage());
            } else {
                log.append("writeHTML: " + e.getMessage(), LogFilePath, "fdp_port_emailer");
            }
        }
    }

    public void DeleteFile(String FileName) {
        try {
            File delF = new File(FileName);
            if (delF.exists()) {
                delF.delete();
                delF.deleteOnExit();
            }
            delF = null;
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "DeleteFile: " + e.getMessage());
            } else {
                log.append("DeleteFile: " + e.getMessage(), LogFilePath, "edi_port_emailer");
            }
        }
    }

    public String SendPort(String Receipent, String CReceipent, String Subject, String Msg, ArrayList Attachment) {
        try {
            //sendF = new SendFile();
            //sendF.setReceipent(Receipent);
            //sendF.setCorbonCopy(CReceipent);
            //sendF.setMsgText(Msg);
            //sendF.setSender(Sender);
            //sendF.setSubject(Subject);
                        
            // new gmailer code.
            gmailer gmail = new gmailer();
            //gmail.setHost("smtpout.europe.secureserver.net");
            gmail.Init("noreply@exchange-data.com","3xch:ang3");
            gmail.setRecipients( Receipent );
            gmail.setBCC( CReceipent );
            gmail.setMessageText( Msg );
            gmail.setSubject( Subject );
            for (int s = 0; s < Attachment.size(); s++) {
                gmail.addAttachement( Attachment.get(s).toString());
            }
                        
            //for (int s = 0; s < Attachment.size(); s++) {
            //    sendF.addAttachment(Attachment.get(s).toString());
            //}
            
            gmail.sendMessage();
            return "sent";
            
            //if (!sendF.sendMessage(Host, Port, UserName, Password)) {
            //    return false;
            //} else {
            //    return true;
            //}            
            
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "SendPort: " + e.getMessage());
            } else {
                log.append("SendPort: " + e.getMessage(), LogFilePath, "edi_port_emailer");
            }
            return e.getMessage();
        }
    }

    public String GetSentenceCase(String str) {
        try {
            str = str.substring(0, 1).toUpperCase() + str.substring(1, str.length()).toLowerCase();
            return str;
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "GetSentenceCase: " + e.getMessage());
            } else {
                log.append("GetSentenceCase: " + e.getMessage(), LogFilePath, "edi_port_emailer");
            }
            return "";
        }
    }

    public void DeletePort(ArrayList DelAttach) {
        try {
            for (int d = 0; d < DelAttach.size(); d++) {
                DeleteFile(DelAttach.get(d).toString());
            }

        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "DeletePort: " + e.getMessage());
            } else {
                log.append("DeletePort:" + e.getMessage(), LogFilePath, "edi_port_emailer");
            }
        }
    }

}
