/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @author h.patel
 */
package EmailAlert.lib;

import com.edi.common.MySqlConnection.MySqlConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


public class db {

    private static Connection conn;

    public db() {
    }

    /***
     * Method:  connect()
     * Purpose: To establish a database connection object with a
     *          mysql server using the custom MySqlConnection method
     *          from edicommon to resolve database host name
     **/
    public void connect(String filename, String Database) {
        conn = new MySqlConnection(filename, Database).getConnection();
    }

    public void CloseConn() {
        if (conn != null) {
            conn = null;
        }
    }

    public int InsertEMTSPrt(int user_id, String dt, String cmt) {
        try {
            Statement Ist = conn.createStatement();
            return Ist.executeUpdate(" INSERT INTO mts_emts.prt_opslog(acttime, user_id, cat_id, prt_dt, comment) "
                    + " VALUES(now()," + user_id + ", 1, '" + dt + "', '" + cmt + "')");
        } catch (Exception e) {
            return 0;
        }
    }

    public ResultSet GetEMTSUserDetail() {
        try {
            ResultSet eu_rs = null;
            Statement eu_st = conn.createStatement();
            eu_rs = eu_st.executeQuery("SELECT mts_emts.user_email_alert.user_id, mts_emts.user.username, "
                    + " mts_emts.firm.firm, mts_emts.user_email_alert.email, "
                    + " mts_emts.user_email_alert.mat_days, "
                    + " mts_emts.user_email_alert.int_days FROM mts_emts.user_email_alert "
                    + " INNER JOIN mts_emts.user ON mts_emts.user_email_alert.user_id =mts_emts.user.user_id "
                    + " INNER JOIN mts_emts.firm ON mts_emts.user.firm_id = mts_emts.firm.firm_id "
                    + " WHERE mts_emts.user_email_alert.active=1");
            return eu_rs;
        } catch (Exception e) {
            return null;
        }
    }

    public ResultSet GetEMTSRefData(int user_id, int mat_days, int int_days) {
        try {
            ResultSet ref_rs = null;
            Statement ref_st = conn.createStatement();
            String sql = "SELECT DISTINCT mts_emts.refdata.ref_date, "
                    + " mts_emts.refdata.isin, mts_emts.refdata.bond_type, "
                    + " mts_emts.refdata.description, mts_emts.refdata.market_code, "
                    + " CASE WHEN (mts_emts.refdata.maturity_date = ADDDATE(CURDATE()," + mat_days + ")) then 'Maturity' "
                    + " ELSE 'Interest Payment' END AS 'Reason' "
                    + " FROM edi_wca.bond "
                    + " INNER JOIN edi_wca.scmst ON edi_wca.bond.secid = edi_wca.scmst.secid  "
                    + " INNER JOIN mts_emts.refdata ON edi_wca.scmst.isin = mts_emts.refdata.isin "
                    + " INNER JOIN mts_emts.user_isin ON mts_emts.refdata.isin=mts_emts.user_isin.isin "
                    + " LEFT OUTER JOIN edi_wca.rd ON edi_wca.bond.secid = edi_wca.rd.secid "
                    + " LEFT OUTER JOIN edi_wca.scexh ON edi_wca.bond.secid = edi_wca.scexh.secid "
                    + " LEFT OUTER JOIN edi_wca.exdt ON edi_wca.rd.rdid = edi_wca.exdt.rdid "
                    + "                       AND edi_wca.scexh.exchgcd = edi_wca.exdt.exchgcd "
                    + "                       AND 'INT' = edi_wca.exdt.eventtype  "
                    + " LEFT OUTER JOIN edi_wca.int_my ON edi_wca.rd.rdid = edi_wca.int_my.rdid "
                    + " LEFT OUTER JOIN edi_wca.intpy ON edi_wca.int_my.rdid = edi_wca.intpy.rdid "
                    + " WHERE "
                    + " (mts_emts.refdata.maturity_date = ADDDATE(CURDATE()," + mat_days + ")) "
                    + " OR "
                    + " (edi_wca.bond.actflag<>'D' "
                    + " AND edi_wca.scmst.actflag<>'D' "
                    + " AND (edi_wca.exdt.paydate = ADDDATE(REPLACE(CURDATE(),'-','/')," + int_days + ") "
                    + " OR CONCAT(EXTRACT(YEAR FROM CURDATE()),SUBSTRING(edi_wca.bond.Interestpaydate1,3,2),SUBSTRING(edi_wca.bond.Interestpaydate1,1,2)) = REPLACE(ADDDATE(CURDATE()," + int_days + "),'-','') "
                    + " OR CONCAT(EXTRACT(YEAR FROM CURDATE()),SUBSTRING(edi_wca.bond.Interestpaydate2,3,2),SUBSTRING(edi_wca.bond.Interestpaydate2,1,2)) = REPLACE(ADDDATE(CURDATE()," + int_days + "),'-','') "
                    + " OR CONCAT(EXTRACT(YEAR FROM CURDATE()),SUBSTRING(edi_wca.bond.Interestpaydate3,3,2),SUBSTRING(edi_wca.bond.Interestpaydate3,1,2)) = REPLACE(ADDDATE(CURDATE()," + int_days + "),'-','') "
                    + " OR CONCAT(EXTRACT(YEAR FROM CURDATE()),SUBSTRING(edi_wca.bond.Interestpaydate4,3,2),SUBSTRING(edi_wca.bond.Interestpaydate4,1,2)) = REPLACE(ADDDATE(CURDATE()," + int_days + "),'-',''))"
                    + " AND (edi_wca.intpy.intrate<>'' OR edi_wca.bond.interestrate<>'') "
                    + " AND ((edi_wca.intpy.intrate<>'' AND edi_wca.intpy.intrate IS NOT NULL) OR edi_wca.bond.interestrate<>'') "
                    + " AND ((edi_wca.intpy.bcurencd <>'' AND edi_wca.intpy.bcurencd IS NOT NULL) OR edi_wca.bond.curencd<>'') "
                    + " AND (mts_emts.refdata.maturity_date >= ADDDATE(CURDATE()," + int_days + ") OR mts_emts.refdata.maturity_date IS NULL) "
                    + " AND (edi_wca.bond.firstcoupondate <= ADDDATE(CURDATE()," + int_days + ") OR edi_wca.bond.firstcoupondate IS NULL) "
                    + " AND (edi_wca.bond.intcommencementdate <= ADDDATE(CURDATE()," + int_days + ") OR edi_wca.bond.intcommencementdate IS NULL) "
                    + " AND (edi_wca.bond.issuedate <= ADDDATE(CURDATE()," + int_days + ") OR edi_wca.bond.issuedate IS NULL)) "
                    + " AND mts_emts.refdata.actflag!= 'D' "
                    + " AND mts_emts.user_isin.user_id= " + user_id
                    + " ORDER BY mts_emts.refdata.acttime DESC ,mts_emts.refdata.isin ASC";
            ref_rs = ref_st.executeQuery(sql);
            /*("SELECT mts_emts.refdata.ref_date, " +
            "  mts_emts.refdata.isin, mts_emts.refdata.bond_type, " +
            "  mts_emts.refdata.description, mts_emts.refdata.market_code " +
            "  FROM  mts_emts.refdata " +
            "  INNER JOIN mts_emts.user_isin ON mts_emts.refdata.isin=mts_emts.user_isin.isin " +
            "           AND mts_emts.refdata.market_code=mts_emts.user_isin.market_code " +
            "           AND mts_emts.refdata.bond_type=mts_emts.user_isin.bond_type " +
            "  WHERE mts_emts.refdata.actflag!= 'D' " +
            "  AND mts_emts.user_isin.user_id= " + user_id +
            "  AND maturity_date >= ADDDATE(CURDATE(),45) " +
            "  ORDER BY mts_emts.refdata.acttime DESC ,mts_emts.refdata.isin ASC");*/
            return ref_rs;
        } catch (Exception e) {
            //System.out.println(e.getMessage());
            return null;
        }
    }

    public ResultSet GetEMTSRefDtDtl(String isin, String MktCD) {
        try {
            ResultSet ref_rs = null;
            Statement ref_st = conn.createStatement();
            ref_rs = ref_st.executeQuery("SELECT * FROM mts_emts.refdata "
                    + "WHERE mts_emts.refdata.isin ='" + isin + "' "
                    + "AND mts_emts.refdata.market_code= '" + MktCD + "' "
                    + "ORDER BY mts_emts.refdata.acttime DESC");
            return ref_rs;
        } catch (Exception e) {
            return null;
        }
    }

    public ResultSet getTasks(int HRS, int MIN) {
        try {
            String sqltext =
                    " SELECT automation.alert_task.* "
                    + " FROM automation.alert_task "
                    + " INNER JOIN automation.alert_schedule ON automation.alert_task.taskid = automation.alert_schedule.taskid "
                    + " WHERE (min=" + MIN + ") AND ( "
                    + " hrs=" + HRS + " OR hrs IS NULL AND  "
                    + " dom IS NULL AND  "
                    + " moy IS NULL AND  "
                    + " dow IS NULL) AND  "
                    + " enabled = 1  "
                    + " Order by priority , prvd_priorty, parametervalue desc ";

            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sqltext);
            return rs;
        } catch (Exception e) {
            return null;
        }
    }

    

    public ResultSet ChkEMTSNewUser(int user_id) {
        try {
            Statement PLst = conn.createStatement();
            ResultSet PLrs = PLst.executeQuery(" SELECT COUNT(*) AS cnt FROM mts_emts.prt_opslog "
                    + " WHERE user_id = " + user_id
                    + " AND cat_id = 1");
            return PLrs;
        } catch (Exception e) {
            return null;
        }
    }

   

    public ResultSet GetEMTSFeedDate(int user_id) {
        try {
            Statement efst = conn.createStatement();
            ResultSet efrs = efst.executeQuery("SELECT DISTINCT mts_emts.opslog.feed_dt "
                    + " FROM mts_emts.opslog "
                    + " INNER JOIN mts_emts.prt_opslog ON mts_emts.opslog.cat_id = mts_emts.prt_opslog.cat_id "
                    + "                           AND mts_emts.prt_opslog.user_id = " + user_id
                    + "                           AND mts_emts.opslog.feed_dt > (SELECT MAX(mts_emts.prt_opslog.prt_dt) FROM mts_emts.prt_opslog WHERE mts_emts.prt_opslog.user_id = " + user_id + ") "
                    + " WHERE mts_emts.opslog.result NOT LIKE 'Holiday' "
                    + " AND mts_emts.opslog.cat_id = 1");
            return efrs;
        } catch (Exception e) {
            return null;
        }
    }

    public ResultSet GetEMTSMaxFeedDate() {
        try {
            Statement efmst = conn.createStatement();
            ResultSet efmrs = efmst.executeQuery("SELECT MAX(feed_dt) AS feed_dt FROM mts_emts.opslog "
                    + " WHERE mts_emts.opslog.result NOT LIKE 'Holiday' "
                    + " AND mts_emts.opslog.cat_id = 1");
            return efmrs;
        } catch (Exception e) {
            return null;
        }
    }

    public ResultSet getIntPay(String ISIN, int int_days) {
        try {
            Statement Ist = conn.createStatement();
            ResultSet Irs = Ist.executeQuery("SELECT DISTINCT ADDDATE(CURDATE(),45) AS Pay_Date, "
                    + " CASE WHEN icur.lookup IS NOT NULL THEN icur.lookup ELSE bcur.lookup END AS Currency, "
                    + " CASE WHEN edi_wca.intpy.intrate<>'' THEN intrate ELSE edi_wca.bond.interestrate END AS Interest_Rate, "
                    + " freq.lookup AS Frequency, "
                    + " intacc.lookup AS Accrual_Convention, "
                    + " frnben.lookup AS FRN_Index_Benchmark, "
                    + " edi_wca.bond.markup AS FRN_Margin, "
                    + " edi_wca.rd.Recdate AS Record_Date, "
                    + " CASE WHEN edi_wca.intpy.rescindinterest IS NULL OR edi_wca.intpy.rescindinterest='F' THEN 'No' ELSE 'Yes' END AS Payment_Cancelled "
                    + " FROM edi_wca.bond "
                    + " INNER JOIN edi_wca.scmst ON edi_wca.bond.secid = edi_wca.scmst.secid "
                    + " INNER JOIN mts_emts.refdata ON edi_wca.scmst.isin = mts_emts.refdata.isin "
                    + " INNER JOIN mts_emts.user_isin ON mts_emts.refdata.isin=mts_emts.user_isin.isin "
                    + " LEFT OUTER JOIN edi_wca.rd ON edi_wca.bond.secid = edi_wca.rd.secid "
                    + " LEFT OUTER JOIN edi_wca.scexh ON edi_wca.bond.secid = edi_wca.scexh.secid "
                    + " LEFT OUTER JOIN edi_wca.exdt ON edi_wca.rd.rdid = edi_wca.exdt.rdid "
                    + "                       AND edi_wca.scexh.exchgcd = edi_wca.exdt.exchgcd "
                    + "                       AND 'INT' = edi_wca.exdt.eventtype "
                    + " LEFT OUTER JOIN edi_wca.int_my ON edi_wca.rd.rdid = edi_wca.int_my.rdid "
                    + " LEFT OUTER JOIN edi_wca.intpy ON edi_wca.int_my.rdid = edi_wca.intpy.rdid "
                    + " LEFT OUTER JOIN edi_wca.tbl_lookup AS icur ON edi_wca.intpy.bcurencd = icur.code "
                    + "                                 AND 'CUREN' = icur.typegroup "
                    + " LEFT OUTER JOIN edi_wca.tbl_lookup AS bcur ON edi_wca.bond.curencd = bcur.code "
                    + "                                 AND 'CUREN' = bcur.typegroup "
                    + " LEFT OUTER JOIN edi_wca.tbl_lookup AS freq ON edi_wca.bond.interestpaymentfrequency = freq.code "
                    + "                                 AND 'FREQ' = freq.typegroup "
                    + " LEFT OUTER JOIN edi_wca.tbl_lookup AS intacc ON edi_wca.bond.interestaccrualconvention = intacc.code "
                    + "                                 AND 'INTACCRUAL' = intacc.typegroup "
                    + " LEFT OUTER JOIN edi_wca.tbl_lookup AS frnben ON edi_wca.bond.frnindexbenchmark = frnben.code "
                    + "                                 AND 'FRNINDXBEN' = frnben.typegroup "
                    + " WHERE edi_wca.bond.actflag<>'D' "
                    + " AND edi_wca.scmst.actflag<>'D' "
                    + " AND (edi_wca.exdt.paydate = ADDDATE(REPLACE(CURDATE(),'-','/')," + int_days + ") "
                    + " OR CONCAT(EXTRACT(YEAR FROM CURDATE()),SUBSTRING(edi_wca.bond.Interestpaydate1,3,2),SUBSTRING(edi_wca.bond.Interestpaydate1,1,2)) = REPLACE(ADDDATE(CURDATE()," + int_days + "),'-','') "
                    + " OR CONCAT(EXTRACT(YEAR FROM CURDATE()),SUBSTRING(edi_wca.bond.Interestpaydate2,3,2),SUBSTRING(edi_wca.bond.Interestpaydate2,1,2)) = REPLACE(ADDDATE(CURDATE()," + int_days + "),'-','') "
                    + " OR CONCAT(EXTRACT(YEAR FROM CURDATE()),SUBSTRING(edi_wca.bond.Interestpaydate3,3,2),SUBSTRING(edi_wca.bond.Interestpaydate3,1,2)) = REPLACE(ADDDATE(CURDATE()," + int_days + "),'-','') "
                    + " OR CONCAT(EXTRACT(YEAR FROM CURDATE()),SUBSTRING(edi_wca.bond.Interestpaydate4,3,2),SUBSTRING(edi_wca.bond.Interestpaydate4,1,2)) = REPLACE(ADDDATE(CURDATE()," + int_days + "),'-','')) "
                    + " AND (edi_wca.intpy.intrate<>'' OR edi_wca.bond.interestrate<>'') "
                    + " AND ((edi_wca.intpy.intrate<>'' AND edi_wca.intpy.intrate IS NOT NULL) OR edi_wca.bond.interestrate<>'') "
                    + " AND ((edi_wca.intpy.bcurencd <>'' AND edi_wca.intpy.bcurencd IS NOT NULL) OR edi_wca.bond.curencd<>'') "
                    + " AND (mts_emts.refdata.maturity_date >= ADDDATE(CURDATE()," + int_days + ") OR mts_emts.refdata.maturity_date IS NULL) "
                    + " AND (edi_wca.bond.firstcoupondate <= ADDDATE(CURDATE()," + int_days + ") OR edi_wca.bond.firstcoupondate IS NULL) "
                    + " AND (edi_wca.bond.intcommencementdate <= ADDDATE(CURDATE()," + int_days + ") OR edi_wca.bond.intcommencementdate IS NULL) "
                    + " AND (edi_wca.bond.issuedate <= ADDDATE(CURDATE()," + int_days + ") OR edi_wca.bond.issuedate IS NULL) "
                    + " AND mts_emts.refdata.isin = '" + ISIN + "'");
            return Irs;
        } catch (Exception e) {
            return null;
        }

    }
}
