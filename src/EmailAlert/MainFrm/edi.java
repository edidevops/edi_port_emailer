/*
 * edi.java
 * Created on 15 September 2008, 11:17
 * @author  h.patel
 */

package EmailAlert.MainFrm;

import EmailAlert.HTMLPorts.ukcab;
import EmailAlert.HTMLPorts.wca;
import EmailAlert.HTMLPorts.wdi;
import EmailAlert.HTMLPorts.wfi;
import EmailAlert.lib.CommonMethods;
import com.edi.common.Logging;
import com.edi.common.mail4EDI.SendFile;
import java.awt.Component;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import com.edi.common.MySqlConnection.MySqlConnection;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Statement;
import java.util.Calendar;
import java.util.List;



public class edi extends javax.swing.JFrame {
                
    CommonMethods cm = new CommonMethods("",0);
    SendFile sendF = new SendFile();
    //gmailer gmail = new gmailer();
    private Logging log = new Logging("Automation");
    private static Connection conn;
    private String sToday = "";    
    private String LogFilePath = "";
    private int bAutoRun = 0;
    private String cmdSrv = null;
    private ArrayList Feed_dt = new ArrayList();
    private String AppPath = System.getProperty("user.dir");
    private String atype="ann";
    private String username="";
    private String prdcd="WCA";
    private int increm=0;
    private int tfrom=0;
    private int tto=0;
    private String ConfigFile = AppPath + "\\edi_port_emailer.cfg";
    private String server = "";
    private List<JCheckBox> cbList = new ArrayList();
    private String url = "https://online.exchange-data.com/";
    
    /** Creates new form EDI */
    public edi(String args[]) {
         
        // Set Look and Feel to Native style.
        try {
	    javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
	 
            initComponents();                          
            //TblPort.setVisible(false);
            
            if(args.length<=0){
                System.out.println("No Parameter found");
                dispose();
            }else{            
                DateFormat sdf =null;
                sdf = new SimpleDateFormat("yyyyMMdd");            
                sToday = sdf.format(new java.util.Date());

                for(int f=0;f<args.length;f++){
                    if(args[f].equalsIgnoreCase("-m")){                   
                        bAutoRun = 1;                    
                    }else if(args[f].equalsIgnoreCase("-a")){
                        BtSend.setEnabled(false);                    
                        bAutoRun = 2;
                    }else if(args[f].equalsIgnoreCase("-s")){
                       //Getting the connection details
                        server = args[f+1];
                    }else if(args[f].equalsIgnoreCase("-l")){
                        LogFilePath = args[f+1];
                    }else if(args[f].equalsIgnoreCase("-dt")){ 
                        Feed_dt.add(args[f+1]);                        
                    }else if(args[f].equalsIgnoreCase("-atype")){
                       atype =args[f+1];
                    }else if(args[f].equalsIgnoreCase("-username")){
                       username =args[f+1];
                    }else if(args[f].equalsIgnoreCase("-prdcd")){
                       prdcd =args[f+1];
                    }else if(args[f].equalsIgnoreCase("-config")){
                       ConfigFile =args[f+1];
                    }else if(args[f].equalsIgnoreCase("-incr")){
                      //Towfik Added for increamental option       
                       increm =Integer.parseInt(args[f+1]);
                    } else if(args[f].equalsIgnoreCase("-tfrom")){
                      //Towfik Added for increamental option       
                       tfrom =Integer.parseInt(args[f+1]);
                    }else if(args[f].equalsIgnoreCase("-tto")){
                      //Towfik Added for increamental option       
                       tto =Integer.parseInt(args[f+1]);
                    }
                }
                   
                OpenDBConnection(server);
                cm = new CommonMethods(LogFilePath, bAutoRun);
                lblStatus.setText(" ");
                PanelStatus.paintImmediately(0,0,PanelStatus.getWidth(), PanelStatus.getHeight());
                if(atype.equalsIgnoreCase("alr")){ setTitle("EDI Portfolio Alert ");}                               
                
                //Populate User Detail in the table.
                PopulateUserDetail();

                try{
                    Thread.sleep(100);
                } catch ( Exception e){	    
                    log.append("(edi)waitFor:" + e.getMessage(), LogFilePath, "edi_port_emailer");
                }

                //Call action performed event in Auto mode.
                if (bAutoRun==2){   
                   BtSendActionPerformed(null);
                   exitForm(null);
                }                
            }                     
        } catch (Exception e){
            log.append("edi:" + e.getMessage(), LogFilePath, "edi_port_emailer");
	} 
    }

    private String [] GetConnDetail(String svr){
      try{
          String [] detail = new String[4];
          String [] tvalues = null;
           File sourcefile = new File(ConfigFile);
            if(sourcefile.exists()){
                BufferedReader in = new BufferedReader(new FileReader(sourcefile));
                String str = null;
                while ((str = in.readLine()) != null) {
                    tvalues = str.split("\t");
                    if(tvalues[0].compareToIgnoreCase(svr)==0){
                        detail[0] = tvalues[1];
                        detail[1] = tvalues[2];
                        detail[2] = tvalues[3];
                        detail[3] = tvalues[4];
                    }
                }
                in.close();
            } else{
                log.append("Could not found the config file: O:\\AUTO\\Configs\\DbServers.cfg",  LogFilePath, "ChkWebAppLog");
            }

          return detail;
      }catch(Exception e){
            if (bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"GetConnDetail: " + e.getMessage());
            }else{
                log.append("GetConnDetail: " + e.getMessage(),  LogFilePath, "ChkWebAppLog");
            }
            return null;
       }
  }
    
     private void OpenDBConnection(String svr){
	try{                  
                String [] connDetail = GetConnDetail(svr);
                conn =new MySqlConnection(connDetail[1],connDetail[2],"edi_cms",connDetail[3]).getConnection();
                cmdSrv = "Mysql";
        }catch (Exception e){            
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"OpenDBConnection: " + e.getMessage());
                exitForm(null);
            }else{                
                log.append("(edi)OpenDBConnection:" + e.getMessage(), LogFilePath, "edi_port_emailer");
                exitForm(null);                
            }                                        
	} 
    }
    
    private void PopulateUserDetail(){
        try{
            ResultSet rs = GetUserDetail();            
            
            if(rs!=null){
                DefaultTableModel model = new DefaultTableModel();

                for (int f=1; f<rs.getMetaData().getColumnCount()+1; f++){
                       // Add field name to table model, but replace any underscore "_" with
                       // a space " "
                    System.out.println(rs.getMetaData().getColumnName(f).replace('_',' '));
                      model.addColumn( rs.getMetaData().getColumnName(f).replace('_',' ') );
                }                        
                model.addColumn("Check");
                while ( rs.next() ){
                    Vector row_data = new Vector();
                    for (int f=1; f<rs.getMetaData().getColumnCount()+1; f++){
                        row_data.addElement( rs.getString(f) );
                    }
                    model.addRow(row_data);                
                }


                TblUser.setModel(model); 
                JCheckBox CB = new JCheckBox();
                CB.setSelected(false);

                CB.setBackground(TblUser.getBackground());
                CB.setHorizontalAlignment(SwingConstants.CENTER);            
                TableColumn includeColumn = TblUser.getColumnModel().getColumn(13);
                //new CheckBoxRenderer().SetSel();
                TblUser.getColumnModel().getColumn(13).setCellRenderer(new CheckBoxRenderer());
                includeColumn.setCellEditor(new DefaultCellEditor(CB));  
                cbList.add(CB);
                //check
                setValue(TblUser);

                //Hide user_id column
//                TblUser.getColumnModel().getColumn(0).setMinWidth(0);
//                TblUser.getColumnModel().getColumn(0).setMaxWidth(0);
//                TblUser.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
//                TblUser.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
//                //Hide product_id column
//                TblUser.getColumnModel().getColumn(3).setMinWidth(0);
//                TblUser.getColumnModel().getColumn(3).setMaxWidth(0);
//                TblUser.getTableHeader().getColumnModel().getColumn(3).setMinWidth(0);
//                TblUser.getTableHeader().getColumnModel().getColumn(3).setMaxWidth(0);
                
                TblUser.setRowSelectionInterval(0,0);
                
                rs.getStatement().close();
                rs.close();
                TblUser.setVisible(true);
                setVisible(true);
            }else{
                if (bAutoRun==0 || bAutoRun==1){
                    javax.swing.JOptionPane.showMessageDialog(null,"No record found");
                    exitForm(null);
                }else{
                    //cm.writelogfile(LogFilePath+sToday +".log","");
                    exitForm(null);                
                }   
            }                        
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"PopulateUserDetail: " + e.getMessage());
                exitForm(null);
            }else{
                log.append("(edi)PopulateUserDetail:" + e.getMessage(), LogFilePath, "edi_port_emailer");
                exitForm(null);                
            }   
        }
    }
    
    
    private void setValue(JTable tbl){
        try{
            for(int f=0; f<tbl.getRowCount(); f++){
                if(TblUser.getValueAt(f, 13) == null)
                    TblUser.setValueAt(true, f, 13);
            }
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"setValue: " + e.getMessage());
                exitForm(null);
            }else{
                log.append("(edi)setValue:" + e.getMessage(), LogFilePath, "edi_port_emailer");
                exitForm(null);                
            }   
        }
        
    }
    
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PanelStatus = new javax.swing.JPanel();
        lblStatus = new javax.swing.JLabel();
        PanelUser = new javax.swing.JPanel();
        ScPanUser = new javax.swing.JScrollPane();
        TblUser = new javax.swing.JTable();
        jcheckSelect = new javax.swing.JCheckBox();
        jLabel2 = new javax.swing.JLabel();
        OverWriteDate = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        PanelPort = new javax.swing.JPanel();
        BtSend = new javax.swing.JButton();
        ScPanPort = new javax.swing.JScrollPane();
        TblPort = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txtOverwrite = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("EDI Portfolio Announcement");
        setAlwaysOnTop(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        lblStatus.setText("Status");

        org.jdesktop.layout.GroupLayout PanelStatusLayout = new org.jdesktop.layout.GroupLayout(PanelStatus);
        PanelStatus.setLayout(PanelStatusLayout);
        PanelStatusLayout.setHorizontalGroup(
            PanelStatusLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(PanelStatusLayout.createSequentialGroup()
                .add(lblStatus, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(27, 27, 27))
        );
        PanelStatusLayout.setVerticalGroup(
            PanelStatusLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(PanelStatusLayout.createSequentialGroup()
                .add(lblStatus, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        PanelUser.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        ScPanUser.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        TblUser.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "UserName", "Firm", "Product", "Email"
            }
        ));
        TblUser.setRequestFocusEnabled(false);
        TblUser.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                TblUserFocusGained(evt);
            }
        });
        TblUser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TblUserMouseClicked(evt);
            }
        });
        TblUser.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TblUserKeyPressed(evt);
            }
        });
        ScPanUser.setViewportView(TblUser);

        jcheckSelect.setSelected(true);
        jcheckSelect.setText("Select All Users");
        jcheckSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcheckSelectActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel2.setText("OverWrite Date (YYYY-MM-DD):");

        OverWriteDate.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        OverWriteDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OverWriteDateActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N

        org.jdesktop.layout.GroupLayout PanelUserLayout = new org.jdesktop.layout.GroupLayout(PanelUser);
        PanelUser.setLayout(PanelUserLayout);
        PanelUserLayout.setHorizontalGroup(
            PanelUserLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(PanelUserLayout.createSequentialGroup()
                .add(jcheckSelect)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(OverWriteDate, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 88, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
            .add(ScPanUser)
        );
        PanelUserLayout.setVerticalGroup(
            PanelUserLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(PanelUserLayout.createSequentialGroup()
                .add(PanelUserLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(PanelUserLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jcheckSelect)
                        .add(jLabel2)
                        .add(OverWriteDate, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(ScPanUser, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 131, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        BtSend.setText("Send");
        BtSend.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        BtSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtSendActionPerformed(evt);
            }
        });

        ScPanPort.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        TblPort.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Portfolio", "Description", "Security"
            }
        ));
        ScPanPort.setViewportView(TblPort);

        org.jdesktop.layout.GroupLayout PanelPortLayout = new org.jdesktop.layout.GroupLayout(PanelPort);
        PanelPort.setLayout(PanelPortLayout);
        PanelPortLayout.setHorizontalGroup(
            PanelPortLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(PanelPortLayout.createSequentialGroup()
                .add(ScPanPort, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(BtSend, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 61, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        PanelPortLayout.setVerticalGroup(
            PanelPortLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(PanelPortLayout.createSequentialGroup()
                .add(11, 11, 11)
                .add(BtSend, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 27, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, PanelPortLayout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(ScPanPort, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 94, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("OverWrite Email:");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(2, 2, 2)
                .add(PanelStatus, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .add(layout.createSequentialGroup()
                .add(PanelPort, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(txtOverwrite, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 368, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
            .add(PanelUser, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(PanelUser, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtOverwrite, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel1))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(PanelPort, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 108, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(PanelStatus, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        setSize(new java.awt.Dimension(642, 364));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
       try{
            if (conn != null) {
                conn = null;
            }
       }catch(Exception e){
          if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"formWindowClosed: " + e.getMessage());
          }else{
              log.append("(edi)formWindowClosed:" + e.getMessage(), LogFilePath, "edi_port_emailer");
          }  
       }
    }//GEN-LAST:event_formWindowClosed

    private void BtSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtSendActionPerformed
        
        try{
            String pub_dt ="";
            String product = "";                        
            int afrom=0;
            int ato=0;
            String prd = "";
            String mail = "";
            String usnme = ""; 
            String filetype = "";
            String delimiter = "";
            String subject  ="";
            int inc_option =0;
            BtSend.setEnabled(false);
            PanelPort.paintImmediately(0,0,PanelPort.getWidth(), PanelPort.getHeight());
            ArrayList Attach = new ArrayList();            
            if(increm!=0){
               if (tfrom==0 && tto==0){
                   pub_dt=getLastIncrement();
                   Feed_dt.add(pub_dt);
               }                
            } 
            //Check whether feed date pass by parameter or not
            if(Feed_dt.size()<=0){
                /*Calendar calendar = Calendar.getInstance();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Feed_dt.add(dateFormat.format(calendar.getTime()));
                } catch (Exception e) {
                      if (bAutoRun==0 || bAutoRun==1){
                            javax.swing.JOptionPane.showMessageDialog(null,"Feed Date: " + e.getMessage());
                      }else{
                          log.append("(edi)Feed Date:" + e.getMessage(), LogFilePath, "edi_port_emailer");
                      }
                }*/                
                ChkFeedDate(prdcd);    
            }
           
          
            //Loop for users
            for(int f=0;f<TblUser.getRowCount();f++){  
              
                jLabel4.setText("Currenly Running On :" + Feed_dt.toString() );
                TblUser.setRowSelectionInterval(f, f);
                populateport();
                PanelUser.paintImmediately(0,0,PanelUser.getWidth(), PanelUser.getHeight());
                lblStatus.setText("Please Wait...., Sending portfolio to " + (TblUser.getValueAt(f, 1)).toString());
                PanelStatus.paintImmediately(0,0,PanelStatus.getWidth(), PanelStatus.getHeight());                                
                
                System.out.println((TblUser.getValueAt(f, 13)).toString());
                
                //Check for enable user
                if((TblUser.getValueAt( f, 13)).toString().compareToIgnoreCase("true")==0){

                    //Loop for different feed dates
                    for(int fd=0; fd<Feed_dt.size(); fd++){
                        filetype = "";
                        delimiter = "";
                        Attach.clear();
                        System.out.println((TblUser.getValueAt( f, 3)).toString());                                               
                        product = TblUser.getValueAt( f, 3).toString();                        
                        if (TblUser.getValueAt( f, 8)!=null){afrom = Integer.parseInt((TblUser.getValueAt( f, 8)).toString());}
                        if (TblUser.getValueAt( f, 9)!=null){ato = Integer.parseInt((TblUser.getValueAt( f, 9)).toString());}
                        prd = (TblUser.getValueAt( f, 4 )).toString().trim();
                        mail = (TblUser.getValueAt( f, 5)).toString().trim();
                        if(!"".equals(txtOverwrite.getText().trim())){ mail=txtOverwrite.getText().trim();} 
                        usnme = (TblUser.getValueAt( f, 1)).toString().trim();
                        if (TblUser.getValueAt( f, 11)!=null){ filetype = (TblUser.getValueAt( f, 11)).toString().trim();}
                        if (TblUser.getValueAt( f, 12)!=null){ delimiter = (TblUser.getValueAt( f, 12)).toString().trim();}
                        inc_option = Integer.parseInt(TblUser.getValueAt( f, 10).toString());  
                        int UId = Integer.parseInt((TblUser.getValueAt( f, 0)).toString());
                        int CId = 0;
                        try{
                            CId = Integer.parseInt((TblUser.getValueAt( f, 7)).toString());
                        }catch(Exception e){
                            CId = 0;
                        }
                        String Codes = (TblUser.getValueAt( f, 6)).toString();
                        System.out.println(mail);
                        int IChk = 0;
                        //Loop for different portfolio
                        for(int f1=0;f1<TblPort.getRowCount();f1++){
                            TblPort.setRowSelectionInterval(f1, f1);
                            PanelPort.paintImmediately(0,0,PanelPort.getWidth(), PanelPort.getHeight());
                           
                            int PId = Integer.parseInt((TblPort.getValueAt(f1, 0)).toString());
                           
                            String filename = AppPath + "\\" + product +"_"+ (TblPort.getValueAt(f1, 1)).toString().replace(" ", "").trim()+ "_" + usnme+ ".htm";
                            if(filetype.equalsIgnoreCase("txt")){
                                filename = filename.replace("htm", "txt");
                            }else if(filetype.equalsIgnoreCase("csv")){
                                filename = filename.replace("htm", "csv");
                            }
                            cm.DeleteFile(filename);
                            String Pnm = (TblPort.getValueAt(f1, 1)).toString().trim();
                            pub_dt =Feed_dt.get(fd).toString();                            
                             //System.out.println(pub_dt);
                            
                            if(!"".equals(OverWriteDate.getText().trim())){ pub_dt=OverWriteDate.getText().trim();} 
                            jLabel4.setText("Currenly Running On :[" + pub_dt.substring(0,10) +  "]");                            
                            if(increm!=0){jLabel4.setText("Currenly Running On :[" + pub_dt.substring(0,10) +  "] Seq: " + increm);}
                             PanelUser.paintImmediately(0,0,PanelUser.getWidth(), PanelUser.getHeight());
                            System.out.println(pub_dt);
                            //Check for product
                            if(product.equalsIgnoreCase("WDI")){
                                wdi twdi = new wdi(LogFilePath, bAutoRun,atype,afrom,ato,inc_option,increm,tfrom,tto,conn, url);
                                //Create portfolio   
                                int pChk = twdi.GetPortfolio(UId, PId, pub_dt, filename, Pnm, Codes,CId,filetype,delimiter);
                                //int pChk =0;
                                if(pChk == 1){
                                    if (bAutoRun==0 || bAutoRun==1){
                                        javax.swing.JOptionPane.showMessageDialog(null,"No update found for portfolio: " + Pnm);
                                    }/*else{
                                        log.append("(edi)No update found for portfolio: " + Pnm, LogFilePath, "edi_port_emailer");
                                    }*/
                                    IChk = 1;
                                }else if(pChk == -1){
                                     if (bAutoRun==0 || bAutoRun==1){
                                        javax.swing.JOptionPane.showMessageDialog(null,"Error generating alert: " + Pnm);
                                    }else{
                                        log.append("(edi)Error generating alert: " + Pnm, LogFilePath, "edi_port_emailer");
                                    }
                                     IChk = -1;
                                }else if(pChk ==0){
                                    Attach.add(filename);
                                    IChk = 0;
                                }
                            }else if(product.equalsIgnoreCase("WCA")){
                                
                                wca twca = new wca(LogFilePath, bAutoRun, atype,afrom,ato,inc_option,increm,tfrom,tto,conn, url);
                                //Create portfolio                               
                                int pChk = twca.GetPortfolio(UId, PId, pub_dt, filename, Pnm, Codes,CId,filetype,delimiter);
                                //int pChk =0;
                                if(pChk == 1){
                                    if (bAutoRun==0 || bAutoRun==1){
                                        javax.swing.JOptionPane.showMessageDialog(null,"No update found for portfolio: " + Pnm);
                                    }/*else{
                                        log.append("(edi)No update found for portfolio: " + Pnm, LogFilePath, "edi_port_emailer");
                                    }*/
                                    IChk = 1;
                                }else if(pChk == -1){
                                     if (bAutoRun==0 || bAutoRun==1){
                                        javax.swing.JOptionPane.showMessageDialog(null,"Error generating alert: " + Pnm);
                                    }else{
                                        log.append("(edi)Error generating alert: " + Pnm, LogFilePath, "edi_port_emailer");
                                    }
                                     IChk = -1;
                                }else if(pChk ==0){
                                    Attach.add(filename);
                                    IChk = 0;
                                }

                            }else if(product.equalsIgnoreCase("ukcab")){                                
                                ukcab tukcab = new ukcab(LogFilePath, bAutoRun,conn, url);
                               //Create portfolio                                
                                int pChk = tukcab.GetPortfolio(UId, PId, pub_dt, filename, Pnm, Codes,CId,filetype,delimiter);
                                //int pChk =0;
                                if(pChk == 1){
                                    if (bAutoRun==0 || bAutoRun==1){
                                        javax.swing.JOptionPane.showMessageDialog(null,"No update found for portfolio: " + Pnm);
                                    }/*else{
                                        log.append("(edi)No update found for portfolio: " + Pnm, LogFilePath, "edi_port_emailer");
                                    }*/
                                    IChk = 1;
                                }else if(pChk == -1){
                                     if (bAutoRun==0 || bAutoRun==1){
                                        javax.swing.JOptionPane.showMessageDialog(null,"Error generating alert: " + Pnm);
                                    }else{
                                        log.append("(edi)Error generating alert: " + Pnm, LogFilePath, "edi_port_emailer");
                                    }
                                     IChk = -1;
                                }else if(pChk ==0){
                                    Attach.add(filename);
                                    IChk = 0;
                                }

                            } else if(product.equalsIgnoreCase("WFI")){
                                
                                wfi twfi = new wfi(LogFilePath, bAutoRun, atype,afrom,ato,inc_option,conn, url);
                                //Create portfolio
                               int pChk = twfi.GetPortfolio(UId, PId, pub_dt, filename, Pnm, Codes,CId,filetype,delimiter);
                                //int pChk =0;
                                if(pChk == 1){
                                    if (bAutoRun==0 || bAutoRun==1){
                                        javax.swing.JOptionPane.showMessageDialog(null,"No update found for portfolio: " + Pnm);
                                    }/*else{
                                        log.append("(edi)No update found for portfolio: " + Pnm, LogFilePath, "edi_port_emailer");
                                    }*/
                                    IChk = 1;
                                }else if(pChk == -1){
                                     if (bAutoRun==0 || bAutoRun==1){
                                        javax.swing.JOptionPane.showMessageDialog(null,"Error generating alert: " + Pnm);
                                    }else{
                                        log.append("(edi)Error generating alert: " + Pnm, LogFilePath, "edi_port_emailer");
                                    }
                                     IChk = -1;
                                }else if(pChk ==0){
                                    Attach.add(filename);
                                    IChk = 0;
                                }

                            }
                            //TblPort.removeRowSelectionInterval(f1, f1);
                            //PanelPort.paintImmediately(0,0,PanelPort.getWidth(), PanelPort.getHeight());
                        }
                        String type = "Announcement";                      
                        if(Attach.size()>0){                            
                            if(atype.equalsIgnoreCase("alr")) type = "Alert";
                            subject = "EDI Portfolio " + type + " - " + prd + " on " + pub_dt + " for " + usnme;
                            if(increm!=0){
                                subject = "EDI Portfolio " + type + " - " + prd + " on " + pub_dt.substring(0,10) + " Incremental: " + increm+ " for " + usnme;
                            }
                            String send = cm.SendPort(mail, "webmaster@exchange-data.com",subject , "", Attach);
                            if(send.equals("sent")){
                                InsertediPrt(Integer.parseInt((TblUser.getValueAt( f, 0)).toString()), (TblUser.getValueAt( f, 3)).toString(), pub_dt, type + " has been sent successfully");
                                
                            }else{
                                if (bAutoRun==0 || bAutoRun==1){
                                    javax.swing.JOptionPane.showMessageDialog(null,"Could not send portfolio");
                              }else{
                                    log.append("(edi)Could not send portfolio", LogFilePath, "edi_port_emailer");
                              }
                              InsertediPrt(Integer.parseInt((TblUser.getValueAt( f, 0)).toString()), (TblUser.getValueAt( f, 3)).toString(), pub_dt, send + ": " + type);
                            }
                        }else{
                          String msg = "";
                          if(IChk == 1){
                              msg = "(edi)No update found for portfolio";
                          }else if(IChk == -1){
                              msg = "(edi)Error generating " + type;
                          }
                          InsertediPrt(Integer.parseInt((TblUser.getValueAt( f, 0)).toString()), (TblUser.getValueAt( f, 3).toString()), pub_dt, msg);
                        }
                       //Delete Portfoliio
                        cm.DeletePort(Attach);
                    }
                }
                TblUser.removeRowSelectionInterval(f, f);
                PanelUser.paintImmediately(0,0,PanelUser.getWidth(), PanelUser.getHeight());
            }
            lblStatus.setText("Portfolios have been sent to clients");
            PanelStatus.paintImmediately(0,0,PanelStatus.getWidth(), PanelStatus.getHeight());            
            BtSend.setEnabled(true);
            PanelPort.paintImmediately(0,0,PanelPort.getWidth(), PanelPort.getHeight());
            if (bAutoRun==2){                        
                exitForm(null);
            }
        }catch(Exception e){
          if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"BtSendActionPerformed: " + e.getMessage());
          }else{
                log.append("(edi)BtSendActionPerformed:" + e.getMessage(), LogFilePath, "edi_port_emailer");
          } 
        }
    }//GEN-LAST:event_BtSendActionPerformed

    private void OverWriteDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OverWriteDateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_OverWriteDateActionPerformed

    private void jcheckSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcheckSelectActionPerformed
        //  Himadri TODO check/uncheck all boxe here:
        if (jcheckSelect.isSelected()){
            TblUser.selectAll();
            int rowCount = TblUser.getRowCount();
            for (int i = 0; i < rowCount; i++){
                TblUser.setValueAt(true, i, 13);
            }

        }else{
            TblUser.clearSelection();
                        int rowCount = TblUser.getRowCount();

            for (int i = 0; i < rowCount; i++){
                TblUser.setValueAt(false, i, 13);
            }
        }
    }//GEN-LAST:event_jcheckSelectActionPerformed

    private void TblUserKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TblUserKeyPressed
        populateport();
    }//GEN-LAST:event_TblUserKeyPressed

    private void TblUserFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_TblUserFocusGained
        populateport();
    }//GEN-LAST:event_TblUserFocusGained

    private void TblUserMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TblUserMouseClicked
        populateport();
    }//GEN-LAST:event_TblUserMouseClicked
  
    private void populateport(){
        try{
            if ( TblUser.getSelectedRow() !=-1 ){
                ResultSet port_rs = GetPortDetail((TblUser.getValueAt( TblUser.getSelectedRow(), 0)).toString(), (TblUser.getValueAt( TblUser.getSelectedRow(), 1)).toString());

                if(port_rs!=null){
                    DefaultTableModel port_model = new DefaultTableModel();

                    for (int f=1; f<port_rs.getMetaData().getColumnCount()+1; f++){
                        // Add field name to table model, but replace any underscore "_" with
                        // a space " "
                        port_model.addColumn( port_rs.getMetaData().getColumnName(f).replace('_',' ') );
                    }

                    while ( port_rs.next() ){
                        Vector row_data = new Vector();
                        for (int f=1; f<port_rs.getMetaData().getColumnCount()+1; f++){
                            row_data.addElement( port_rs.getString(f) );
                        }
                        port_model.addRow(row_data);
                    }
                    TblPort.setModel(port_model);
                    //Hide port_id
                    TblPort.getColumnModel().getColumn(0).setMinWidth(0);
                    TblPort.getColumnModel().getColumn(0).setMaxWidth(0);
                    TblPort.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
                    TblPort.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);                   
                    //TblPort.setVisible(true);
                    port_rs.getStatement().close();
                    port_rs.close();
                }else{
                    if (bAutoRun==0 || bAutoRun==1){
                        javax.swing.JOptionPane.showMessageDialog(null,"No portfolio found");
                    }else{
                        //cm.writelogfile(LogFilePath+sToday +".log", "");
                    }
                }
            }
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"TblUserFocusGained: " + e.getMessage());
            }else{
                log.append("(edi)TblUserFocusGained:" + e.getMessage(), LogFilePath, "edi_port_emailer");
            }
        }
    }

    private void exitForm(java.awt.event.WindowEvent evt) {       
        if (conn != null) {
            conn = null;
        }
        dispose();
        //System.exit(0);        
    }
    
    private void ChkFeedDate(String  prod_cd){
        try{
            Feed_dt.clear();                        
            String sSql = "";
            if(prod_cd.equalsIgnoreCase("ukcab")){
                sSql="SELECT  DATE_FORMAT(Max(Acttime),'%Y-%m-%d') AS Load_Date FROM wol.Cab WHERE cabStatus = 'SENT'";
            }else{
                sSql= "Select  DATE_FORMAT(Max(feeddate),'%Y-%m-%d') As Load_Date From wca2.tbl_opslog order by acttime desc;";
            }
            ResultSet Nrs = null;
            Statement St = conn.createStatement(); 
            Nrs = St.executeQuery(sSql);
	    if(Nrs !=null){
                while(Nrs.next()){
                   Feed_dt.add(Nrs.getString("Load_Date"));                     
                }
	    }
           
            Nrs.close();            
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"ChkFeedDate: " + e.getMessage());
            }else{
                log.append("(edi)ChkFeedDate:" + e.getMessage(), LogFilePath, "edi_port_emailer");
            } 
        }
    }

    private ResultSet GetUserDetail() {
        try {
            ResultSet Urs = null;
            Statement Ust = conn.createStatement();
            String sSql = "SELECT distinct   U.user_num, "+
                    " U.username AS UserName, "+
                    " F.company AS Firm,P.prod_cd,P.product AS Product, "+
                    " E.email AS Email, "+
                    " C.Codes,CN.contact_id,"+
                    " UP.afrom,UP.ato,UP.inc_option,E.file_type,E.delimiter_type"+
                    " FROM es12105.pass_file AS U  "+
                    " left outer JOIN edi_cms.contact AS CN ON U.contact_id = CN.contact_id  "+
                    " left outer JOIN edi_cms.company AS F ON CN.company_id = F.company_id "+
                    " left outer JOIN portfolio.profile As E ON U.user_num = E .user_id  "+
                    " left outer JOIN portfolio.alert_prd As UP ON U.user_num = UP .user_id  "+
                    " left outer JOIN edi_cms.products AS P  ON  UP.prod_cd=P.prod_cd "+
                    " INNER JOIN es12105.user_prd AS UA ON U.user_num=UA.user_id and UP.prod_cd= UA.prod_cd And UA.active=1"+
                    " LEFT OUTER JOIN (Select distinct prod_cd, "+
                    " GROUP_CONCAT(DISTINCT code "+
                    " ORDER BY code DESC SEPARATOR ',' ) As Codes "+
                    " from portfolio.port_type "+
                    " inner join portfolio.mtx_port_type on port_type.cd_id=mtx_port_type.cd_id "+
                    " where active=1 "+
                    " GROUP BY prod_cd) AS C ON P.prod_cd  =C.prod_cd "+
                    " WHERE U.account_is_live = 1  and  E.email != '' "+
                    " And U.user_num IN (SELECT Distinct user_id from portfolio.lstportfolio where alert ='Y' and actflag!='D ')";
            sSql+= " and UP.atype like '%"+atype+"%'";
            if(username!=""){sSql+= " and U.username ='" + username + "'" ;}
            if(prdcd!=""){sSql+= " and P.prod_cd ='" + prdcd + "'" ;}
            //Towfik Added for increamental option 
            if(increm!=0){
                sSql+= " and UP.incr = 1 " ;
            }else{
                 sSql+= " and UP.incr != 1 " ; 
            }
            sSql+=  " ORDER BY U.user_num , P.prod_cd asc";           
            Urs = Ust.executeQuery(sSql);           
            return Urs;
        } catch (Exception e) {
            return null;
        }
    }

    private int InsertediPrt(int user_id, String product_cd, String dt, String cmt) {
        try {
            Statement Ist = conn.createStatement();
            String sql = " INSERT INTO portfolio.opslog(acttime, user_id, prod_cd, prt_dt,type, comment) "
                    + " VALUES(now()," + user_id + ", '" + product_cd + "', '" + dt + "','" + atype + "','" + cmt + "')";
            return Ist.executeUpdate(sql);
        } catch (Exception e) {
            return 0;
        }
    }

    private ResultSet GetPortDetail(String user, String cd) {
        try {
            ResultSet Prs = null;
            Statement Pst = conn.createStatement();
            String psql= "SELECT port_id, " +
                                   " port, " +
                                   " port_desc " +
                                   " FROM portfolio.lstportfolio " +
                                   " WHERE user_id = " + user + " AND alert ='Y' And actflag !='D'" +                                   
                                   " Order by   port asc";
            Prs = Pst.executeQuery(psql);
            return Prs;
        } catch (Exception e) {
            return null;
        }
    }
    private String getLastIncrement(){
      String rtn = "";
      try{
        ResultSet Rs = null;
        Statement St = conn.createStatement(); 
        Rs = St.executeQuery("select max(acttime) As increm from wca2.tbl_opslog where seq ="+increm);
	    if(Rs !=null){
                while(Rs.next()){
                    rtn = Rs.getString("increm");                      
                }
	    }
	    Rs.close();
	    St.close();
        return rtn;
     
      }catch(Exception e){
        return rtn;
     }    
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {       
        new edi(args);//.setVisible(true);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtSend;
    private javax.swing.JTextField OverWriteDate;
    private javax.swing.JPanel PanelPort;
    private javax.swing.JPanel PanelStatus;
    private javax.swing.JPanel PanelUser;
    private javax.swing.JScrollPane ScPanPort;
    private javax.swing.JScrollPane ScPanUser;
    private javax.swing.JTable TblPort;
    private javax.swing.JTable TblUser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JCheckBox jcheckSelect;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JTextField txtOverwrite;
    // End of variables declaration//GEN-END:variables
    
}
class CheckBoxRenderer extends DefaultTableCellRenderer {
  JCheckBox checkBox = new JCheckBox();   
  

    @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
      
      boolean bool = false;
      
      try{
        bool = Boolean.getBoolean( (String)value );
      }catch(Exception e){
          //System.err.println(e.getMessage());
      }
      
      checkBox.setSelected(value != null && ((Boolean) value).booleanValue());       
      checkBox.setHorizontalAlignment(SwingConstants.LEFT);
      checkBox.setBackground(table.getBackground());
      return checkBox;
  }      
}










