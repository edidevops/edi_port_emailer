/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @author h.patel
 */

package EmailAlert.HTMLPorts;

import EmailAlert.lib.CommonMethods;
import EmailAlert.lib.db;
import com.edi.common.Logging;
import com.edi.common.MsSqlConnection.MsSqlConnection;
import java.sql.ResultSet;
import java.util.ArrayList;
import com.edi.common.MySqlConnection.MySqlConnection;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.Statement;


public class wso {

    db db_com = new db();
    CommonMethods cm = new CommonMethods("",0);
    private static Connection conn;
    private static Connection conn2;
    private Logging log = new Logging("Automation");    
    private String LogFilePath = "";
    private int bAutoRun = 0;
    private String AppPath = System.getProperty("user.dir");
    private boolean isdata = false;
    
    public wso(String Log, int Auto, Connection conn1){
        cm = new CommonMethods(Log,Auto);
        LogFilePath = Log;
        bAutoRun = Auto;
        //conn = new MySqlConnection(AppPath + "\\fdp_port_emailer.cfg", "fdp_prt").getConnection();
        conn = conn1;
    }

    public int GetPortfolio(int usr_id, int port_id, String pub_dt, String port_nm, String pname, String Codes, String dbe, String cfgfile){
        try{
            SecConn(dbe, cfgfile);
            cm.DeleteFile(port_nm);
            ArrayList caref = new ArrayList();
            cm.writeHTML(port_nm, "<html lang='en'> " +
                        "<head> " +
                        "<title>FinDataPortal- EDI Worldwide Shares Outstanding </title> " +
                        "</head> " +
                        "<body background-color='#FFFFF8'>  " +
                        "<table border=0 width=820px align=left bgcolor=white> " +
                        "<tr><td align=center><P><b>EDI Worldwide Shares Outstanding Email Notification - " + pname + " - " + pub_dt + "<b></p></td></tr> " +
                        "<tr><td align=center><P><b>Portfolio Summary<b></p></td></tr> " +
                        "<tr> " +
                        "<td nowrap> ");
            
            //Get Summary
            ResultSet hrs = GetWSOPortSummary(usr_id, port_id, pub_dt, Codes);
            if(hrs != null){
                cm.writeHTML(port_nm, "<table border=0 width=820px align=left bgcolor=white><tr bgcolor=darkGray>" +
                          "<td><b>EventID</b></td>" +                          
                          "<td><b>IssuerName</b></td>" +
                          "<td><b>Incorp</b></td>" +
                          "<td><b>Exchange</b></td>" +                          
                          "<td><b>Issue</b></td>" +
                          "</tr>");
                while(hrs.next()){                    
                    
                    cm.writeHTML(port_nm, "<tr bgcolor=white>" +
                                             "<td><A HREF='#"+hrs.getString("caref")+"'>" + hrs.getString("EventID") + "</a></td>" +
                                             "<td>" + hrs.getString("IssuerName") + "</td>" +
                                             "<td>" + hrs.getString("Incorp") + "</td>" +
                                             "<td>" + hrs.getString("Exchange") + "</td>" +
                                             "<td>" + hrs.getString("Issue") + "</td>" +
                                             "</tr>");
                    caref.add(hrs.getString("caref"));
                    
                }
            }else{
                if (bAutoRun==0 || bAutoRun==1){
                    javax.swing.JOptionPane.showMessageDialog(null,"Could not get Portfolio Summary");                   
                }else{                
                    log.append("(FDP)Could not get Portfolio Summary", LogFilePath, "fdp_port_emailer");
                }
                cm.DeleteFile(port_nm);
                return 1;
            }            
            cm.writeHTML(port_nm, "</table>");
            cm.writeHTML(port_nm, "</td></tr><tr><td><hr></td></tr>");
            hrs.close();                        
            
            //Start writing portfolio detail
            String [] field = {"Choice"  ,  "IssID"  , "Parvalue"  ,  "RegCountry"  ,  "SecGrpID"  ,  "SecID"  ,  "SecurityDescriptor"};

            for(int f=0; f<caref.size(); f++){
                    ResultSet port_rs = GetWSOPortfolio(caref.get(f).toString());
                    if(port_rs!=null){
                        //addhtml = printdata(field, port_rs);
                        //port_rs.beforeFirst();
                       // cm.writeHTML(port_nm,"<tr><td>");
                        while(port_rs.next()){
                            cm.writeHTML(port_nm,"<tr><td><A Name='"+port_rs.getString("caref")+"'></a>");
                            cm.writeHTML(port_nm, "<table align=center width=98% border=0 cellpading=0 width=95% cellspacing=5> " +
                                                " <tr>" +
                                                " <td><b>EventID:</b></td><td> " + port_rs.getString("EventID") + "</td>" +
                                                " </tr>" +
                                                " <tr>" +
                                                "                 <td><b>Created:</b></td><td> " + formatDate(port_rs.getString("Created")) + "</td>" +
                                                "                 <td><b>Changed:</b></td><td> " + formatDate(port_rs.getString("Changed")) + "</td>" +
                                                "                 <td><b>ActFlag:</b></td><td> " + port_rs.getString("ActFlag") + "</td>" +
                                                "  " +
                                                " </tr>" +
                                                " <tr>" +
                                                "                 <td><b>IssuerName:</b></td><td colspan=3> " + port_rs.getString("IssuerName") + "</td>" +
                                                "                 <td><b>CntryofIncorp:</b></td><td> " + port_rs.getString("CntryofIncorp") + "</td>" +
                                                " </tr>" +
                                                " <tr>" +
                                                "  " +
                                                "                 <td><b>SecurityDesc:</b></td><td colspan=3> " + port_rs.getString("SecurityDesc") + "</td>" +
                                                "                 <td><b>PVCurrency:</b></td><td> " + port_rs.getString("PVCurrency") + "</td>" +
                                                " </tr>" +
                                                " <tr>" +
                                                "                 <td><b>ISIN:</b></td><td> " + port_rs.getString("ISIN") + "</td>" +
                                                "                 <td><b>UsCode:</b></td><td> " + port_rs.getString("UsCode") + "</td>" +
                                                "  " +
                                                "                 <td><b>Sedol:</b></td><td> " + port_rs.getString("Sedol") + "</td>" +
                                                "  " +
                                                " </tr>" +
                                                " <tr>" +
                                                "                 <td><b>Localcode:</b></td><td> " + port_rs.getString("Localcode") + "</td>" +
                                                "                 <td><b>Status:</b></td><td> " + port_rs.getString("StatusFlag") + "</td>" +
                                                "                 <td><b>SecGroup:</b></td><td > " + port_rs.getString("grpname") + "</td>" +
                                                "  " +
                                                " </tr>" +
                                                " <tr>" +
                                                "                 <td><b>SecType:</b></td><td nowrap> " + port_rs.getString("SecTyCD") + " - " + port_rs.getString("SecurityDesc") + "</td>" +
                                                "                 <td><b>MIC:</b></td><td> " + port_rs.getString("MIC") + "</td>" +
                                                "                 <td><b>PrimaryEx:</b></td><td> " + port_rs.getString("PrimaryEx") + "</td>" +
                                                " </tr>" +
                                                " <tr>" +
                                                "                 <td><b>ExchgCD:</b></td><td nowrap colspan=3> " + port_rs.getString("ExchgCD") + " - " + port_rs.getString("ExchgName") + "</td>" +
                                                "                 <td><b>ExCountry:</b></td><td> " + port_rs.getString("ExCountry") + "</td>" +
                                                " </tr>" +
                                                " <tr><td colspan=6 align=center><br>" +
                                                " <table border=0 width=95% align=center cellspacing='2'  cellpadding='2'>" +
                                                "   <tr bgcolor = darkGray>" +
                                                "   <th valign=top nowrap align=center>Effective Date</th> " +
                                                "   <th valign=top nowrap align=center>Old Shares</th>  " +
                                                "   <th valign=top nowrap align=center>New Shares</th>  " +
                                                "   <th valign=top nowrap align=center>List Status</th> " +
                                                "   <th valign=top nowrap align=center>List Date</th> " +
                                                "   </tr>" +
                                                "   <tbody><TR bgColor=white  >" +
                                                "   <td valign =top  nowrap align=center> " + formatDate(port_rs.getString("EffectiveDate")) + "</td>" +
                                                "   <td valign =top  nowrap align=center> " + removetrailingzero(port_rs.getString("OldSharesoutstanding")) + "</td>" +
                                                "   <td valign =top  nowrap align=center> " + removetrailingzero(port_rs.getString("NewSharesOutstanding")) + "</td>" +
                                                "   <td valign =top  nowrap align=center> " + port_rs.getString("ListStatus") + "</td>" +
                                                "  " +
                                                "   <td valign =top  nowrap align=center  > " + formatDate(port_rs.getString("Listdate")) + "</td>" +
                                                "   </tr></tbody>" +
                                                " </table>" +
                                                " <br></td>" +
                                                " </tr>");
                                                String shtml = "";
                                                int cnt =1;
                                                for(int z=0;z<field.length;z++){
                                                    if(port_rs.getString(field[z]) != null){
                                                        String data = port_rs.getString(field[z]);
                                                       if(field[z].equals("Parvalue")){
                                                            data = removetrailingzero(port_rs.getString(field[z]));
                                                        }
                                                        if(!data.trim().equals("")){
                                                            if(cnt == 2){
                                                                shtml+= "<td><b>" + field[z] + ":</b></td><td nowrap> " + data + "</td>";
                                                                cnt = cnt+1;
                                                            }else if(cnt == 1){
                                                               shtml+= "<tr><td><b>" + field[z] + ":</b></td><td nowrap> " + data + "</td>";
                                                               cnt = cnt+1;
                                                            }else if(cnt == 3){
                                                               shtml+= "<td><b>" + field[z] + ":</b></td><td nowrap> " + data + "</td></tr>";
                                                               cnt = 1;
                                                            }
                                                        }
                                                    }
                                                }
                                                cm.writeHTML(port_nm,shtml);                                                                                               
                            cm.writeHTML(port_nm,/*" <tr>" +

                                                "                 <td valign = top><b>RecordDateNotes:</b></td><td colspan=6><p style=white-space:normal> "  + getNotes(port_rs.getString("Link_RecordDateNotes"), port_rs.getString("EventID")) + " </p></td> " +
                                               // "                    <td><b>Notes:</b></td><td nowrap> No Notes </td>" +
                                                " </tr>" +
                                                " <tr>" +                                                        
                                                "                 <td valign = top><b>Notes:</b></td><td colspan=6><p style=white-space:normal>  " + getNotes(port_rs.getString("Link_Notes"), port_rs.getString("EventID"))  + " </p></td>" +
                                                // "                  <td><b>Notes:</b></td><td nowrap> No Notes </td>" +
                                                " </tr>" +*/
                                                " </table></td></tr>");
                            cm.writeHTML(port_nm,"<tr><td><A href='#top'>Go to Top</a></td></tr><tr><td><hr></td></tr> ");
                        }
                    }else{
                        if (bAutoRun==0 || bAutoRun==1){
                            javax.swing.JOptionPane.showMessageDialog(null,"Could not get Portfolio Detail header");                   
                        }else{                
                            log.append("(FDP)Could not get Portfolio Detail header", LogFilePath, "fdp_port_emailer");
                        }
                        cm.DeleteFile(port_nm);
                        return 1;
                    }
                    port_rs.close();
                }                                
            
            
           cm.writeHTML(port_nm, "</table></body></html>");
           CloseConn();
           return 0; 
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"GetPortfolio: " + e.getMessage());                   
            }else{                
                log.append("(FDP)GetPortfolio: " + e.getMessage(), LogFilePath, "fdp_port_emailer");
            }
            cm.DeleteFile(port_nm);
            CloseConn();
            return -1;
        }
    }

     private ResultSet GetWSOPortSummary(int usr_id, int port_id, String pub_dt, String Code) {
        try {
            String[] cd = Code.split(",");
            ResultSet Srs = null;
            Statement Sst = conn2.createStatement();
            //towfik made some changes
            String sql = "USE wca2; Select distinct Caref , EventID , IssuerName, " +
                         " CntryofIncorp AS Incorp,ExchgCd AS Exchange,SecurityDesc AS Issue " +
                         " from evf_shares_outstanding_change " +
                         " Where ((Changed  Between '" + pub_dt + "' And '" + pub_dt + " 23:59:59') " +
                         " OR (Created Between '" + pub_dt + "' And '" + pub_dt + " 23:59:59')) " +
                         " And ExCountry!= 'US' And ExCountry!= 'C'" +
                         " And ( ";

           //repeat with this PortType
           for(int i=0; i < cd.length; i++){
               sql += cd[i] + " IN ( SELECT code FROM OPENQUERY(FINDATA,'Select code  from fdp_prt." + cd[i] +
                              " where actflag != ''D'' And port_id=" + port_id + " and usr_id= " + usr_id + "')) OR ";
            }
            sql =  sql.substring(0,sql.length()-3);
            sql += ") Order by  IssuerName asc,EventID Desc";
            Srs = Sst.executeQuery(sql);
            return Srs;
            
        } catch (Exception e) {
            return null;
        }
    }


    private ResultSet GetWSOPortfolio(String CAREF) {
        try {
            ResultSet Drs = null;
            Statement Dst = conn2.createStatement();
            Drs = Dst.executeQuery("Use WCA; " +
                                  " Select  D.*, " +
                                  " SecTyGrp.SecurityDescriptor,SecTyGrp.grpname,EXCHG.ExchgName " +
                                  " from wca2.dbo.evf_shares_outstanding_change AS D " +
                                  " Left Outer Join  sectyGrp On D.sectycd = sectyGrp.sectycd " +
                                  " Left Outer Join  EXCHG On  D.exchgcd = EXCHG.exchgcd " +
                                  " Where Caref=" + CAREF);
            return Drs;
        } catch (Exception e) {
            return null;
        }
    }   

    private void CloseConn() {
    /*    if (conn != null) {
            conn = null;
        }*/

        if(conn2 != null){
            conn2 = null;
        }
    }

    private void SecConn(String svr, String filename){
        conn2 = new MsSqlConnection("sa","9Wp3:1ti","wca2","66.165.133.153").getConnection();
        /*String [] tvalues=null;
        File sourcefile = new File(AppPath + "\\"+ filename);
        try {
            if(sourcefile.exists()){
                BufferedReader in = new BufferedReader(new FileReader(sourcefile));
                String str = null;
                while ((str = in.readLine()) != null) {
                    //if(str.indexOf(svr)>=0)
                    tvalues = str.split("\t");
                    if(tvalues[0].compareToIgnoreCase(svr)==0){
                        driver = tvalues[1];
                        user = tvalues[2];
                        pass = tvalues[3];
                        database = tvalues[4];
                        host = tvalues[5];
                        conn2 = new MsSqlConnection("sa","9Wp3:1ti","wca2","66.165.133.153").getConnection();                         
                    }
                }
                in.close();
            } else{
                if (bAutoRun==0 || bAutoRun==1){
                    javax.swing.JOptionPane.showMessageDialog(null,"Could not Find File(smfloader.cfg)");
                }else{
                    log.append("Could not Find File(smfloader.cfg)",  LogFilePath, "smfLoader");
                }
            }
        } catch (Exception e) {
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"getConnDetail: " + e.getMessage());
            }else{
                log.append("getConnDetail: " + e.getMessage(),  LogFilePath, "smfLoader");
            }
        }*/
    }

    private String removetrailingzero(String str){
        try{
            if(str.contains(".")){
                while(str.endsWith("0")){
                    str = str.substring(0, str.length()-1);
                }
                if(str.endsWith(".")) str = str.substring(0,str.length()-1);
                return str;
            }else{
                return str;
            }
        }catch(Exception e){
            return "";
        }
    }

    private String processRatio(String str){
        try{
            if(str.equals(":")){
                return "";
            }else{
                String [] tstr = str.split(":");
                if(tstr[0].contains(".")){
                    while(tstr[0].endsWith("0")){
                        tstr[0] = tstr[0].substring(0, tstr[0].length()-1);
                    }
                    if(tstr[0].endsWith(".")) tstr[0] = tstr[0].substring(0,tstr[0].length()-1);
                }
                if(tstr[1].contains(".")){
                    while(tstr[1].endsWith("0")){
                        tstr[1] = tstr[1].substring(0, tstr[1].length()-1);
                    }
                    if(tstr[1].endsWith(".")) tstr[1] = tstr[1].substring(0,tstr[1].length()-1);
                }
                str = tstr[0] + ":" + tstr[1];
                return str;
            }
        }catch(Exception e){
            return "";
        }
    }

    private String formatDate(String str){
        try{

            if(str != null){
                if(str.length() > 10){
                    str = str.substring(0,10);
                    return str;
                }else{
                    return "";
                }
            }else{
                return "";
            }

        }catch(Exception e){
            return "";
        }
    }

   
}
