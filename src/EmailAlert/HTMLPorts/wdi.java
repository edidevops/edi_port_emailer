/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @author h.patel
 */

package EmailAlert.HTMLPorts;

import EmailAlert.lib.CommonMethods;
import EmailAlert.lib.db;
import com.edi.common.Logging;
import java.sql.ResultSet;
import java.util.ArrayList;
import com.edi.common.MySqlConnection.MySqlConnection;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class wdi {

    db db_com = new db();
    CommonMethods cm = new CommonMethods("",0);
    private static Connection conn;   
    private Logging log = new Logging("Automation");    
    private String LogFilePath = "";
    private int bAutoRun = 0;
    private String AppPath = System.getProperty("user.dir");
    private String atype = "ann";
    private int afrom=0;
    private int ato=0 ;
    private int inc_option=0;
    private boolean isdata  = false;
    private int increm=0;
    private int tfrom=0;
    private int tto=0;
    private String url = "";
    
    public wdi(String Log, int Auto,String atyp,int af,int at,int ioption,int incr,int tmfrom, int tmto, Connection conn1, String url){
        cm = new CommonMethods(Log,Auto);
        LogFilePath = Log;
        bAutoRun = Auto;
        atype= atyp;
        afrom= af;
        ato= at;
        inc_option = ioption;
        //conn = new MySqlConnection(AppPath + "\\edi_port_emailer.cfg", "portfolio").getConnection();
        conn = conn1;
        increm= incr;
        tfrom= tmfrom;
        tto= tmto;
        this.url = url;
    }



    public int GetPortfolio(int usr_id, int port_id, String pub_dt, String port_nm, String pname, String Codes,int CId,String filetype, String delimiter){
        try{
            boolean isHTML = true;               
            String type = "Announcement";
            if(atype.equalsIgnoreCase("alr")) type = "Alert";
            cm.DeleteFile(port_nm);
            ArrayList caref = new ArrayList();
            if(filetype.equalsIgnoreCase("txt")){
                isHTML = false;
                if(delimiter.contains("\\t")){
                    delimiter = "\t";
                }                
            }else if(filetype.equalsIgnoreCase("csv")){
                isHTML = false;
                delimiter = ",";                
            }
            String header = pub_dt;
            if(increm!=0){
                header = pub_dt.substring(0,10) + " Incremental: " + increm;
            }
            if(isHTML){
                cm.writeHTML(port_nm, "<html lang='en'> " +
                            "<head> " +
                            "<title>EDI Worldwide Dividends  Email " + type + " </title> " +
                            "</head> " +
                            "<body background-color='#FFFFF8'>  " +
                            "<table border=0 width=820px align=left bgcolor=white> " +
                            "<tr><td align=center><P><b>EDI Worldwide Dividends Email " + type + " - " + pname + " - " + header + "<b></p></td></tr> " +
                            "<tr><td align=center><P><b>Portfolio Summary<b></p></td></tr> " +
                            "<tr> " +
                            "<td nowrap> ");
            }else if(filetype.equalsIgnoreCase("txt")){
                 cm.writeFile(port_nm, "EDI Worldwide Dividends Email " + type + " - " + pname + " - " + header);                
            }else if(filetype.equalsIgnoreCase("csv")){
                 cm.writeFile(port_nm, "\"EDI Worldwide Dividends Email " + type + " - " + pname + " - " + header + "\"");
            }
            
            
            //Get Summary
            ResultSet hrs = GetWDIPortSummary(usr_id, port_id, pub_dt, Codes);
            if(hrs != null){
                if(isHTML){
                    cm.writeHTML(port_nm, "<table border=0 width=98% align=left bgcolor='#E8E8E8'><tr bgcolor=darkGray>" +
                              "<td><b>DivID</b></td>" +
                              "<td><b>DividendType</b></td>" +
                              "<td><b>IssuerName</b></td>" +
                              "<td><b>Incorp</b></td>" +                                                                              
                              "<td><b>Div Period</b></td>" +
                              "<td><b>Ex date</b></td>" +
                              "<td><b>Record date</b></td>" +
                              "<td><b>Pay date</b></td>" +
                              "<td><b>ExchangeCD</b></td>" +
                              "<td><b>PrimaryExchange</b></td>" +
                              "<td><b>Currency</b></td>" +
                              "<td><b>ISIN</b></td>" +
                              "<td><b>SEDOL</b></td>" +
                              "<td><b>USCODE</b></td>" +
                              "<td><b>Actionflag</b></td>" +
                              "<td><b>Gross</b></td>" +
                              "<td><b>Net</b></td>" +
                              "<td><b>Ratio</b></td>" +
                               "</tr>");
                }else if(filetype.equalsIgnoreCase("txt")){
                    cm.writeFile(port_nm, "DivID" + delimiter + "DividendType" + delimiter + "IssuerName" + delimiter + "Incorp" + delimiter + 
                                          "Div Period" + delimiter + "Ex date" + delimiter + "Record date" + delimiter + "Pay date" + delimiter + 
                                          "ExchangeCD" + delimiter + "PrimaryExchange" + delimiter + "Currency" + delimiter + "ISIN" + delimiter + 
                                          "SEDOL" + delimiter + "USCODE" + delimiter + "Actionflag" + delimiter + "Gross" + delimiter + 
                                          "Net" + delimiter + "Ratio");

                }else if(filetype.equalsIgnoreCase("csv")){
                    cm.writeFile(port_nm, "\"DivID\"" + delimiter + "\"DividendType\"" + delimiter + "\"IssuerName\"" + delimiter + "\"Incorp\"" + delimiter + 
                                          "\"Div Period\"" + delimiter + "\"Ex date\"" + delimiter + "\"Record date\"" + delimiter + "\"Pay date\"" + delimiter + 
                                          "\"ExchangeCD\"" + delimiter + "\"PrimaryExchange\"" + delimiter + "\"Currency\"" + delimiter + "\"ISIN\"" + delimiter + 
                                          "\"SEDOL\"" + delimiter + "\"USCODE\"" + delimiter + "\"Actionflag\"" + delimiter + "\"Gross\"" + delimiter + 
                                          "\"Net\"" + delimiter + "\"Ratio\"");

                }
                
                while(hrs.next()){                    
                    if(isHTML){
                        cm.writeHTML(port_nm, "<tr bgcolor=white>" +
                                "<td align=center><A HREF='"+url+"/wca/full_view.php"+
                                "?Caref="+hrs.getString("caref")+"&sedol="+hrs.getString("sedol")+"&event=dividend&eventid="+hrs.getString("eventid")+"&secid="+hrs.getString("secid")+"&exchgcd="+hrs.getString("exchange_code")+
                                "&uid="+usr_id+"&cid="+CId+"' target='blank'>" + hrs.getString("eventid") + "</a></td>" + 
                                "<td align=center>" + capitalize(hrs.getString("dividend_type")) + "</td>" +
                                "<td>" + hrs.getString("issuer_name") + "</td>" +
                                "<td align=center>" + hrs.getString("country_of_incorporation") + "</td>" +
                                "<td align=center>" + capitalize(hrs.getString("dividend_period")) + "</td>" +
                                "<td align=center>" + formatDate(hrs.getString("ex_date")) + "</td>" +
                                "<td align=center>" + formatDate(hrs.getString("record_date")) + "</td>" +
                                "<td align=center>" + formatDate(hrs.getString("pay_date")) + "</td>" +
                                "<td align=center>" + hrs.getString("exchange_code") + "</td>" +
                                "<td align=center>" + capitalize(hrs.getString("primary_exchange")) + "</td>" +
                                "<td align=center>" + hrs.getString("currency") + "</td>" +
                                "<td align=center>" + hrs.getString("isin") + "</td>" +
                                "<td align=center>" + hrs.getString("sedol") + "</td>" +
                                "<td align=center>" + hrs.getString("us_code") + "</td>" +
                                "<td align=center>" + hrs.getString("actflag") + "</td>" +
                                "<td align=center>" + hrs.getString("gross_dividend") + "</td>" +
                                "<td align=center>" + hrs.getString("net_dividend") + "</td>" +
                                "<td align=center>" + hrs.getString("ratio") + "</td>" +
                                "</tr>");
                    }else if(filetype.equalsIgnoreCase("txt")){
                        cm.writeFile(port_nm, hrs.getString("caref")+ delimiter + capitalize(hrs.getString("dividend_type")) + delimiter + 
                                     hrs.getString("issuer_name") + delimiter + hrs.getString("country_of_incorporation") + 
                                     delimiter + capitalize(hrs.getString("dividend_period")) + delimiter + 
                                     formatDate(hrs.getString("ex_date")) + delimiter + formatDate(hrs.getString("record_date")) + 
                                     delimiter + formatDate(hrs.getString("pay_date")) + delimiter + hrs.getString("exchange_code") + 
                                     delimiter + capitalize(hrs.getString("primary_exchange")) + delimiter + 
                                     hrs.getString("currency") + delimiter + hrs.getString("isin") + delimiter + 
                                     hrs.getString("sedol") + delimiter + hrs.getString("us_code") + delimiter + 
                                     hrs.getString("actflag") + delimiter + hrs.getString("gross_dividend") + delimiter + 
                                     hrs.getString("net_dividend") + delimiter + hrs.getString("ratio") + "\"");
                    }else if(filetype.equalsIgnoreCase("csv")){
                        cm.writeFile(port_nm, 
                                     "\"" + hrs.getString("caref") + "\"" + delimiter + "\"" + capitalize(hrs.getString("dividend_type")) + "\"" + delimiter + 
                                     "\"" + hrs.getString("issuer_name") + "\"" + delimiter + "\"" + hrs.getString("country_of_incorporation") + "\"" + delimiter + 
                                     "\"" + capitalize(hrs.getString("dividend_period")) + "\"" + delimiter + "\"" + formatDate(hrs.getString("ex_date")) + "\"" + delimiter + 
                                     "\"" +  formatDate(hrs.getString("record_date")) + "\"" + delimiter + "\"" + formatDate(hrs.getString("pay_date")) + "\"" + delimiter + 
                                     "\"" + hrs.getString("exchange_code") + "\"" + delimiter + "\"" + capitalize(hrs.getString("primary_exchange")) + "\"" + delimiter +
                                     "\"" + hrs.getString("currency") + "\"" + delimiter + "\"" + hrs.getString("isin") + "\"" + delimiter + 
                                     "\"" + hrs.getString("sedol") + "\"" + delimiter + "\"" + hrs.getString("us_code") + "\"" + delimiter + 
                                     "\"" + hrs.getString("actflag") + "\"" + delimiter + "\"" + hrs.getString("gross_dividend") + "\"" + delimiter + 
                                     "\"" + hrs.getString("net_dividend") + "\"" + delimiter + "\"" + hrs.getString("ratio") + "\"");

                    }
                    //caref.add(hrs.getString("caref"));
                    isdata = true;
                    
                }
            }else{
                if (bAutoRun==0 || bAutoRun==1){
                    javax.swing.JOptionPane.showMessageDialog(null,"Could not get Portfolio Summary");                   
                }else{                
                    log.append("(EDI)Could not get Portfolio Summary", LogFilePath, "edi_port_emailer");
                }
                cm.DeleteFile(port_nm);
                return 1;
            }            
            if(isHTML){
                cm.writeHTML(port_nm, "</table>");
                cm.writeHTML(port_nm, "</td></tr><tr><td><hr></td></tr>");
            }else if(filetype.equalsIgnoreCase("txt")){
                 cm.writeFile(port_nm, "EDI_ENDOFFILE");                
            }else if(filetype.equalsIgnoreCase("csv")){
                 cm.writeFile(port_nm, "\"EDI_ENDOFFILE\"");
            }
            hrs.close();                        
                       
           if(!isdata){
               
               if(isHTML){
                   cm.DeleteFile(port_nm);
                   cm.writeHTML(port_nm, "<html lang='en'> " +
                            "<head> " +
                            "<title>EDI Worldwide Dividends  Email " + type + " </title> " +
                            "</head> " +
                            "<body background-color='#FFFFF8'>  " +
                            "<table border=0 width=820px align=left bgcolor=white> " +
                            "<tr><td align=center><P><b>EDI Worldwide Dividends Email " + type + " - " + pname + " - " + pub_dt + "<b></p></td></tr> " +
                            "<tr><td align=center><P><b>No Record Match Your Portfolio<b></p></td></tr> ");
               }
           }
           if(isHTML){
                cm.writeHTML(port_nm, "</table></body></html>");
           }
           return 0; 
        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"GetPortfolio: " + e.getMessage());                   
            }else{                
                log.append("(EDI)GetPortfolio: " + e.getMessage(), LogFilePath, "edi_port_emailer");
            }
            cm.DeleteFile(port_nm);            
            return -1;
        }
    }

     private ResultSet GetWDIPortSummary(int usr_id, int port_id, String pub_dt, String Code) {
        try {
            String[] cd = Code.split(",");
            ResultSet Srs = null;
            Statement Sst = conn.createStatement();
            String WhereClouse="";
            if(atype.equalsIgnoreCase("alr")){
               WhereClouse = GetAlrtWhereClouse(usr_id, pub_dt).toString();
            }else{
               //Towfik Added for increamental option             
                 if (tfrom!=0 && tto!=0){                
                    WhereClouse= " Where (changed BETWEEN '" + pub_dt + " " + tfrom + ":00:00' AND '" + pub_dt +" "+ tto + ":00:00' "+
                          " OR created BETWEEN '" + pub_dt + " " + tfrom + ":00:00' AND '" + pub_dt +" "+ tto + ":00:00') ";
                }else if(increm==0){
                    WhereClouse= " Where (changed BETWEEN '" + pub_dt + "' AND '" + pub_dt + " 23:59:59' "+
                            " OR   created BETWEEN '" + pub_dt + "' AND '" + pub_dt + " 23:59:59') ";
                }else{
                    WhereClouse= " Where (changed >= '" + pub_dt + "' "+
                          " OR created >= '" + pub_dt + "' )";  
                }       
            }
            String sql = "Select distinct D.* from wca2.evf_dividend AS D";
            sql+= WhereClouse + " And ( ";

           //repeat with this PortType
           for(int i=0; i < cd.length; i++){
               String tcode = "";
               if(cd[i].equalsIgnoreCase("uscode")) tcode = "us_code";
               else tcode = cd[i];
               sql += tcode + " IN (Select Distinct codes  from portfolio." + cd[i] +
                              " where port_id=" + port_id + " and user_id= " + usr_id + ") OR ";
            }
            sql =  sql.substring(0,sql.length()-3);
            sql += ") ";
            if (inc_option==0){sql += " And primary_exchange = 'Yes' ";}
            sql += " Order by  issuer_name asc,eventid Desc";
            Srs = Sst.executeQuery(sql);
            return Srs;
            
        } catch (Exception e) {
            return null;
        }
    }
     
private  String GetAlrtWhereClouse(int usr_id,String pub_dt) {
       String whr="";
        try {
            ResultSet Srs = null;
            Statement Sst = conn.createStatement();
            String sql="";
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            
             //Get pub_dt1
            c.setTime(sdf.parse(pub_dt));
            c.add(Calendar.DATE, afrom);  
            String pub_dt1 = sdf.format(c.getTime());

            //Get pub_dt2
            c.setTime(sdf.parse(pub_dt));
            c.add(Calendar.DATE, ato);  
            String pub_dt2 = sdf.format(c.getTime());
            
            sql = "Select dte_type from portfolio.wdi_alrt  Where user_id="+ usr_id;
            Srs = Sst.executeQuery(sql);
	    if(Srs !=null){
                whr+=" Where (";
		while(Srs.next()){              
                    whr+=" ("+ Srs.getString("dte_type").toString() + " Between '" + pub_dt1 + "' And '" + pub_dt2 + " 23:59:59') OR "; 
                                               
		}
                whr =  whr.substring(0,whr.length()-3);
                whr += ")";
	    }
	    Srs.close();
	    Sst.close();
        } catch (Exception e) {
	    if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"GetEvent: " + e.getMessage());
            }else{
                log.append("(EDI)GetEvent: " + e.getMessage(), LogFilePath, "edi_port_emailer");
            }
        }
        return whr;
    }
    private ResultSet GetWDIPortfolio(String CAREF) {
        try {
            ResultSet Drs = null;
            Statement Dst = conn.createStatement();
            Drs = Dst.executeQuery(" Select  D.*, " +
                                   " sectygrp.SecurityDescriptor,sectygrp.grpname,exchg.ExchgName " +
                                   " from wca2.evf_dividend AS D " +
                                   " Left Outer Join  wca.sectygrp On D.security_type = sectygrp.sectycd " +
                                   " Left Outer Join  wca.exchg On  D.exchange_code = exchg.exchgcd " +
                                   " Where Caref=" + CAREF);
            return Drs;
        } catch (Exception e) {
            return null;
        }
    }   

    private String removetrailingzero(String str){
        try{
            if(str.contains(".")){
                while(str.endsWith("0")){
                    str = str.substring(0, str.length()-1);
                }
                if(str.endsWith(".")) str = str.substring(0,str.length()-1);
                return str;
            }else{
                return str;
            }
        }catch(Exception e){
            return "";
        }
    }

    private String processRatio(String str){
        try{
            if(str.equals(":")){
                return "";
            }else{
                String [] tstr = str.split(":");
                if(tstr[0].contains(".")){
                    while(tstr[0].endsWith("0")){
                        tstr[0] = tstr[0].substring(0, tstr[0].length()-1);
                    }
                    if(tstr[0].endsWith(".")) tstr[0] = tstr[0].substring(0,tstr[0].length()-1);
                }
                if(tstr[1].contains(".")){
                    while(tstr[1].endsWith("0")){
                        tstr[1] = tstr[1].substring(0, tstr[1].length()-1);
                    }
                    if(tstr[1].endsWith(".")) tstr[1] = tstr[1].substring(0,tstr[1].length()-1);
                }
                str = tstr[0] + ":" + tstr[1];
                return str;
            }
        }catch(Exception e){
            return "";
        }
    }

    private String formatDate(String str){
        try{

            if(str != null){
                if(str.length() > 10){
                    str = str.substring(0,10);
                    return str;
                }else{
                    return "";
                }
            }else{
                return "";
            }

        }catch(Exception e){
            return "";
        }
    }

    private String capitalize(String s){
        try{
            String [] letter = s.split("_");
            s = "";
            for(int f=0;f<letter.length;f++){
                s += letter[f].substring(0,1).toUpperCase() + letter[f].substring(1);
            }            
            return s;
        }catch(Exception e){
            return "";
        }
    }

   
}
