/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @author h.patel
 */
package EmailAlert.HTMLPorts;

import EmailAlert.lib.CommonMethods;
import com.edi.common.Logging;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import com.edi.common.MySqlConnection.MySqlConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class wfi {

    CommonMethods cm = new CommonMethods("", 0);
    private Logging log = new Logging("Automation");
    private static Connection conn;
    private String LogFilePath = "";
    private int bAutoRun = 0;
    private String AppPath = System.getProperty("user.dir");
    private ArrayList event = new ArrayList();
    private ArrayList dte_type = new ArrayList();
    private int afrom = 0;
    private int ato = 0;
    private int inc_option = 0;
    private String atype = "alr";
    private boolean isdata = false;
    private int increm = 0;
    private String url = "";

    public wfi(String Log, int Auto, String atyp, int af, int at, int ioption, Connection conn1, String url) {
        cm = new CommonMethods(Log, Auto);
        LogFilePath = Log;
        bAutoRun = Auto;
        // conn = new MySqlConnection(AppPath + "\\edi_port_emailer.cfg", "portfolio").getConnection();
        conn = conn1;
        afrom = af;
        ato = at;
        inc_option = ioption;
        atype = atyp;
        this.url = url;

    }

    public int GetPortfolio(int usr_id, int port_id, String pub_dt, String port_nm, String pname, String Codes, int CId, String filetype, String delimiter) {
        try {
            boolean isHTML = true;
            if (filetype.equalsIgnoreCase("txt")) {
                isHTML = false;
                if (delimiter.contains("\\t")) {
                    delimiter = "\t";
                }
            } else if (filetype.equalsIgnoreCase("csv")) {
                isHTML = false;
                delimiter = ",";
            }
            String content = "";
            String type = "Announcement";
            if (atype.equalsIgnoreCase("alr")) {
                type = "Alert";
            }
            cm.DeleteFile(port_nm);
            if (isHTML) {
                cm.writeHTML(port_nm, "<html lang='en'> "
                    + "<head> "
                    + "<title>EDI Worldwide Fixed Income Email " + type + " </title> "
                    + "</head> "
                    + "<body background-color='#FFFFF8'>  "
                    + "<table border=0 width=99% top=99% align=left bgcolor=white> "
                    + "<tr><td align=center><P><br>EDI Worldwide Fixed Income Email " + type + " - " + pname + " - " + pub_dt + "<b></p></td></tr> "
                    + "<tr><td align=center><P><b>Portfolio Summary<b></p></td></tr> "
                    + "<tr> "
                    + "<td nowrap> ");
            } else if (filetype.equalsIgnoreCase("txt")) {
                cm.writeFile(port_nm, "EDI Worldwide Fixed Income Email " + type + " - " + pname + " - " + pub_dt);
            } else if (filetype.equalsIgnoreCase("csv")) {
                cm.writeFile(port_nm, "\"EDI Worldwide Fixed Income Email " + type + " - " + pname + " - " + pub_dt + "\"");
            }
            //Get Summary
            if (atype.equalsIgnoreCase("alr")) {
                GetEventALR(usr_id);
            } else {
                GetEventANN(usr_id);
            }
            if (event.size() > 0) {
                if (isHTML) {
                    content = "<table border=0 width=99% top=99% align=left bgcolor='#E8E8E8'><tr bgcolor=darkGray>";
                    if (atype.equalsIgnoreCase("alr")) {
                        content += "<td><b>SecID</b></td>"
                            + "<td><b>Event</b></td>"
                            + "<td><b>ISIN</b></td>"
                            + "<td><b>Issuer</b></td>"
                            + "<td><b>Description</b></td>"
                            + "<td><b>Exchange</b></td>"
                            + "<td nowrap><b>Calendar Date</b></td>"
                            + "<td nowrap><b>Effective On</b></td>";
                    } else {
                        content += "<td><b>EventID</b></td>"
                            + "<td><b>Event</b></td>"
                            + "<td><b>ISIN</b></td>"
                            + "<td><b>Issuer</b></td>"
                            + "<td><b>Description</b></td>"
                            + "<td><b>Exchange</b></td>";
                    }
                    content += "</tr>";

                    cm.writeHTML(port_nm, content);
                } else if (filetype.equalsIgnoreCase("txt")) {
                    if (atype.equalsIgnoreCase("alr")) {
                        content = "SecID" + delimiter + "Event" + delimiter + "ISIN" + delimiter
                            + "Issuer" + delimiter + "Description" + delimiter + "Exchange" + delimiter + "Calendar Date" + delimiter
                            + "Effective On";
                    } else {
                        content = "EventID" + delimiter + "Event" + delimiter + "ISIN" + delimiter
                            + "Issuer" + delimiter + "Description" + delimiter + "Exchange";
                    }
                    cm.writeFile(port_nm, content);
                } else if (filetype.equalsIgnoreCase("csv")) {
                    if (atype.equalsIgnoreCase("alr")) {
                        content = "\"SecID\"" + delimiter + "\"Event\"" + delimiter + "\"ISIN\"" + delimiter
                            + "\"Issuer\"" + delimiter + "\"Description\"" + delimiter + "\"Exchange\"" + delimiter
                            + "\"Calendar Date\"" + delimiter + "\"Effective On\"";
                    } else {
                        content = "\"EventID\"" + delimiter + "\"Event\"" + delimiter + "\"ISIN\"" + delimiter
                            + "\"Issuer\"" + delimiter + "\"Description\"" + delimiter + "\"Exchange\"";
                    }
                    cm.writeFile(port_nm, content);
                }
                for (int z = 0; z < event.size(); z++) {
                    ResultSet hrs = null;
                    if (atype.equalsIgnoreCase("alr")) {
                        hrs = GetPortSummaryALR(event.get(z).toString(), pub_dt, usr_id, port_id, dte_type.get(z).toString());
                    } else {
                        hrs = GetPortSummaryAnn(event.get(z).toString(), pub_dt, usr_id, port_id);
                    }
                    if (hrs != null) {
                        while (hrs.next()) {
                            if (isHTML) {
                                if (atype.equalsIgnoreCase("alr")) {
                                    content = "<tr bgcolor=white>"
                                        + "<td><A HREF='"+url+"/wfi/full_view.php"
                                        + "?SecId=" + hrs.getString("secid") + "&exchgcd=" + hrs.getString("exchgcd") + "&issuer=" + hrs.getString("issuer")
                                        + "&uid=" + usr_id + "&cid=" + CId + "' target='blank'>" + hrs.getString("secid") + "</a></td>"
                                        + "<td >" + capitalize(hrs.getString("event")) + "</td>"
                                        + "<td align=center>" + hrs.getString("isin") + "</td>"
                                        + "<td>" + hrs.getString("issuer") + "</td>"
                                        + "<td>" + hrs.getString("description") + "</td>"
                                        + "<td align=center>" + hrs.getString("ExchgCD") + "</td>"
                                        + "<td nowrap align=center>" + capitalize(hrs.getString("calendar_date")) + "</td>"
                                        + "<td nowrap align=center>" + hrs.getString("effective_on").substring(0, 10) + "</td>"
                                        + "</tr>";
                                } else {
                                    content = "<tr bgcolor=white>"
                                        + "<td><A HREF='"+url+"/wfi2/port_full.php"
                                        + "?SecId=" + hrs.getString("secid") + "&exchgcd=" + hrs.getString("exchgcd") + "&issuer=" + hrs.getString("issuer")
                                        + "&uid=" + usr_id + "&cid=" + CId  
                                        + "&firef=" +  hrs.getString("firef") +  "&tbl=" + hrs.getString("event")  
                                        + "' target='blank'>" + hrs.getString("EventID") + "</a></td>"
                                        + "<td >" + capitalize(hrs.getString("event")) + "</td>"
                                        + "<td align=center>" + hrs.getString("isin") + "</td>"
                                        + "<td>" + hrs.getString("issuer") + "</td>"
                                        + "<td>" + hrs.getString("description") + "</td>"
                                        + "<td align=center>" + hrs.getString("ExchgCD") + "</td>"
                                        + "</tr>";
                                }
                                cm.writeHTML(port_nm, content);
                            } else if (filetype.equalsIgnoreCase("txt")) {
                                if (atype.equalsIgnoreCase("alr")) {
                                    content = hrs.getString("secid") + delimiter + capitalize(hrs.getString("event")) + delimiter + hrs.getString("isin") + delimiter
                                        + hrs.getString("issuer") + delimiter + hrs.getString("description") + delimiter + hrs.getString("ExchgCD") + delimiter + capitalize(hrs.getString("calendar_date"))
                                        + hrs.getString("effective_on");
                                } else {
                                    content = hrs.getString("secid") + delimiter + capitalize(hrs.getString("event")) + delimiter + hrs.getString("isin") + delimiter
                                        + hrs.getString("issuer") + delimiter + hrs.getString("description") + delimiter + hrs.getString("ExchgCD");
                                }
                                cm.writeFile(port_nm, content);
                            } else if (filetype.equalsIgnoreCase("csv")) {
                                if (atype.equalsIgnoreCase("alr")) {
                                    content = "\"" + hrs.getString("secid") + "\"" + delimiter + "\"" + capitalize(hrs.getString("event")) + "\"" + delimiter + "\"" + hrs.getString("isin") + "\"" + delimiter
                                        + "\"" + hrs.getString("issuer") + "\"" + delimiter + "\"" + hrs.getString("description") + "\"" + delimiter + "\"" + hrs.getString("ExchgCD") + delimiter + "\"" + capitalize(hrs.getString("calendar_date"))
                                        + delimiter + "\"" + hrs.getString("effective_on");
                                } else {
                                    content = "\"" + hrs.getString("secid") + "\"" + delimiter + "\"" + capitalize(hrs.getString("event")) + "\"" + delimiter + "\"" + hrs.getString("isin") + "\"" + delimiter
                                        + "\"" + hrs.getString("issuer") + "\"" + delimiter + "\"" + hrs.getString("description") + "\"" + delimiter + "\"" + hrs.getString("ExchgCD");
                                }
                                cm.writeFile(port_nm, content);
                            }
                            isdata = true;

                        }
                    }
                    hrs.close();
                }
                if (isHTML) {
                    cm.writeHTML(port_nm, "</table>");
                    cm.writeHTML(port_nm, "</td></tr><tr><td><hr></td></tr>");
                } else if (filetype.equalsIgnoreCase("txt")) {
                    cm.writeFile(port_nm, "EDI_ENDOFFILE");
                } else if (filetype.equalsIgnoreCase("csv")) {
                    cm.writeFile(port_nm, "\"EDI_ENDOFFILE\"");
                }
            }
            if (!isdata) {

                if (isHTML) {
                    cm.DeleteFile(port_nm);
                    cm.writeHTML(port_nm, "<html lang='en'> "
                        + "<head> "
                        + "<title>EDI Worldwide Fixed Income Email " + type + " </title> "
                        + "</head> "
                        + "<body background-color='#FFFFF8'>  "
                        + "<table border=0 width=99% top=99% align=left bgcolor=white> "
                        + "<tr><td align=center><P><br>EDI Worldwide Fixed Income Email " + type + " - " + pname + " - " + pub_dt + "<b></p></td></tr> "
                        + "<tr><td align=center><P><b>No Record Match Your Portfolio<b></p></td></tr> ");
                }
            }
            if (isHTML) {
                cm.writeHTML(port_nm, "</table></body></html>");
            }
            //CloseConn();
            return 0;

        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "GetPortfolio: " + e.getMessage());
            } else {
                log.append("(EDI)GetPortfolio: " + e.getMessage(), LogFilePath, "edi_port_emailer");
            }
            cm.DeleteFile(port_nm);
            //CloseConn();
            return -1;
        }

    }

    private void GetEventALR(int usr_id) {
        try {
            ResultSet Srs = null;
            Statement Sst = conn.createStatement();
            String sql = "";
            sql = "Select Event, dte_type from portfolio.wfi_event  Where user_id=" + usr_id + " and type='alr' Order by Event Asc";
            Srs = Sst.executeQuery(sql);
            if (Srs != null) {
                while (Srs.next()) {
                    event.add(Srs.getString("Event"));
                    dte_type.add(Srs.getString("dte_type"));
                }
            }
            Srs.close();
            Sst.close();
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "GetEvent: " + e.getMessage());
            } else {
                log.append("(EDI)GetEvent: " + e.getMessage(), LogFilePath, "edi_port_emailer");
            }
        }
    }

    private void GetEventANN(int usr_id) {
        try {
            ResultSet Srs = null;
            Statement Sst = conn.createStatement();
            String sql = "";
            sql = "Select Event from portfolio.wfi_event  Where user_id=" + usr_id + " and type='ann' Order by Event Asc";
            Srs = Sst.executeQuery(sql);
            if (Srs != null) {
                while (Srs.next()) {
                    event.add(Srs.getString("Event"));
                    dte_type.add(Srs.getString("dte_type"));
                }
            }
            if (event.isEmpty()) {                
                sql = " Select tbl_name As  Event from prdxtra.wfi_list_of_events where port_alert = 1 Order by orderby Asc";
                Srs = Sst.executeQuery(sql);
                if (Srs != null) {
                    while (Srs.next()) {
                        event.add(Srs.getString("Event").replace("wfi.", ""));
                    }
                }
            }
            Srs.close();
            Sst.close();
        } catch (Exception e) {
            if (bAutoRun == 0 || bAutoRun == 1) {
                javax.swing.JOptionPane.showMessageDialog(null, "GetEvent: " + e.getMessage());
            } else {
                log.append("(EDI)GetEvent: " + e.getMessage(), LogFilePath, "edi_port_emailer");
            }
        }
    }

    private ResultSet GetPortSummaryAnn(String event, String pub_dt, int usr_id, int port_id) {
        try {
            ResultSet Drs = null;
            Statement Dst = conn.createStatement();
            String Sql = " Select  DISTINCT '" + event + "'  AS Event,S.SecID As EventID, S.SecID,S.isin,I.issuername As issuer,S.SecurityDesc As description, S.PrimaryExchgCD As ExchgCD,firef "
                + "From wfi." + event + " As R "
                + "Inner join wca.Scmst As S On R.SecId=S.SecId "
                + "Inner join wca.Issur As I On S.IssId=I.IssId "
                + "left outer join wca.exchg As E on S.PrimaryExchgCD = E.exchgcd "
                + "Left Outer join wca.Sedol As SD On R.SecId=SD.SecId And E.CntryCd=SD.CntryCd ";
            if (increm == 0) {
                Sql += " Where (changed BETWEEN '" + pub_dt + "' AND '" + pub_dt + " 23:59:59' "
                    + " OR created BETWEEN '" + pub_dt + "' AND '" + pub_dt + " 23:59:59') ";
            } else {
                Sql += " Where (changed = '" + pub_dt + "' "
                    + " OR created = '" + pub_dt + "' )";
            }
            Sql += " And ( "
                + " uscode IN (Select codes  from portfolio.uscode where port_id= " + port_id + " and user_id= " + usr_id + ") OR  "
                + " sedol IN (Select codes  from portfolio.sedol where  port_id= " + port_id + " and user_id=  " + usr_id + ") OR  "
                + " isin IN (Select codes  from portfolio.isin where  port_id= " + port_id + " and user_id= " + usr_id + ")) ";


            Sql += " And S.PrimaryExchgCD=E.exchgcd Order by issuername asc ,S.SecId  asc";
            Drs = Dst.executeQuery(Sql);
            return Drs;
        } catch (Exception e) {
            return null;
        }
    }

    private ResultSet GetPortSummaryALR(String event, String pub_dt, int usr_id, int port_id, String dte_type) {
        String Sql = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();

            //Get pub_dt1
            c.setTime(sdf.parse(pub_dt));
            c.add(Calendar.DATE, afrom);
            String pub_dt1 = sdf.format(c.getTime());

            //Get pub_dt2
            c.setTime(sdf.parse(pub_dt));
            c.add(Calendar.DATE, ato);
            String pub_dt2 = sdf.format(c.getTime());

            //Create WHERE Clause for date type
            String[] dte = dte_type.split(",");
            String Wclause = "";
            for (int z = 0; z < dte.length; z++) {
                Wclause = " Where (" + dte[z] + " BETWEEN '" + pub_dt1 + "' AND '" + pub_dt2 + " 23:59:59') ";
                if (event.equalsIgnoreCase("redemption")) {
                    Wclause += " And redemption_type!='Maturity'";
                }
                Sql += " Select  '" + event + "'  AS Event, Scmst.SecID,Scmst.isin,issur.issuername As issuer,Scmst.SecurityDesc As description, Scmst.PrimaryExchgCD As ExchgCD,'" + dte[z] + "' AS calendar_date," + dte[z] + " AS effective_on "
                    + " From wfi." + event
                    + " Inner join wca.Scmst On wfi." + event + ".SecId=Scmst.SecId"
                    + " Inner join wca.Issur On Scmst.IssId=Issur.IssId "
                    + " Left Outer join wca.Sedol On wfi." + event + ".SecId=Sedol.SecId "
                    + Wclause
                    + " And ( uscode IN (Select codes  from portfolio.uscode where  port_id=" + port_id + "  and user_id= " + usr_id + ") OR  "
                    + " sedol IN (Select codes  from portfolio.sedol where  port_id= " + port_id + "  and user_id= " + usr_id + " ) OR  "
                    + " isin IN (Select codes  from portfolio.isin where port_id=" + port_id + " and user_id= " + usr_id + "))"
                    + " UNION";
            }
            Sql = Sql.substring(0, Sql.length() - 5);
            //Sql+= " Order by issuername asc ,Scmst.SecID Desc; ";
            ResultSet Drs = null;
            Statement Dst = conn.createStatement();
            Drs = Dst.executeQuery(Sql);
            return Drs;
        } catch (Exception e) {
            return null;
        }
    }

    private String capitalize(String s) {
        try {
            String[] letter = null;
            if (s.contains("_")) {
                letter = s.split("_");
            } else {
                letter = s.split(" ");
            }
            s = "";
            for (int f = 0; f < letter.length; f++) {
                s += letter[f].substring(0, 1).toUpperCase() + letter[f].substring(1) + " ";
            }
            s = s.substring(0, s.length() - 1);
            return s;
        } catch (Exception e) {
            return "";
        }
    }
}
