/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @author h.patel
 */

package EmailAlert.HTMLPorts;

import EmailAlert.lib.CommonMethods;
import EmailAlert.lib.db;
import com.edi.common.Logging;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import com.edi.common.MySqlConnection.MySqlConnection;


public class ukcab {

    CommonMethods cm = new CommonMethods("",0);
    private Logging log = new Logging("Automation");
    private static Connection conn;
    private String LogFilePath = "";
    private int bAutoRun = 0;
    private String AppPath = System.getProperty("user.dir");
    private ArrayList event = new ArrayList();
    private boolean isdata = false;
    private String url = "http://www.exchange-data.com";

    public ukcab(String Log, int Auto,Connection conn1, String url){
        cm = new CommonMethods(Log,Auto);
        LogFilePath = Log;
        bAutoRun = Auto;
        //conn = new MySqlConnection(AppPath + "\\edi_port_emailer.cfg", "portfolio").getConnection();
        conn = conn1;
        this.url = url;
    }

    public int GetPortfolio(int usr_id, int port_id, String pub_dt, String port_nm, String pname, String Codes,int CId, String filetype, String delimiter){
        try{
            boolean isHTML = true; 
            if(filetype.equalsIgnoreCase("txt")){
                isHTML = false;
                if(delimiter.contains("\\t")){
                    delimiter = "\t";
                }               
            }else if(filetype.equalsIgnoreCase("csv")){
                isHTML = false;
                delimiter = ",";                
            }
            cm.DeleteFile(port_nm);
            if(isHTML){
                cm.writeHTML(port_nm, "<html lang='en'> " +
                            "<head> " +
                            "<title>EDI UK Corporate Actions Bulletin (WinCAB) Email Notification </title> " +
                            "</head> " +
                            "<body background-color='#FFFFF8'>  " +
                            "<table border=0 width=820px align=left bgcolor=white> " +
                            "<tr><td align=center><P><b>EDI UK Corporate Actions Bulletin (WinCAB) Email Announcement  - " + pname + " - " + pub_dt + "<b></p></td></tr> " +
                            "<tr><td align=center><P><b>Portfolio Summary<b></p></td></tr> " +
                            "<tr> " +
                            "<td nowrap> ");
            }else if(filetype.equalsIgnoreCase("txt")){
                 cm.writeFile(port_nm, "EDI UK Corporate Actions Bulletin (WinCAB) Email Announcement - " + pname + " - " + pub_dt);                
            }else if(filetype.equalsIgnoreCase("csv")){
                 cm.writeFile(port_nm, "\"EDI UK Corporate Actions Bulletin (WinCAB) Email Announcement - " + pname + " - " + pub_dt + "\"");
            }
                //Get Summary
		//GetEvent();
               
                    
                            ResultSet hrs = GetukcabPortSummary( pub_dt, usr_id, port_id);
                            if(hrs != null){
                                if(isHTML){
                                    cm.writeHTML(port_nm, "<table border=0 width=98% align=left bgcolor='#E8E8E8'><tr bgcolor=darkGray>" +
                                              "<td><b>Issuer Name</b></td>" +                                          
                                              "<td><b>ISIN</b></td>" +
                                              "<td><b>Sedol</b></td>" +
                                              "<td><b>CABID</b></td>" +
                                              "<td><b>Situation</b></td>" +
                                              "<td><b>DeliveryTimeGMT</b></td>" +
                                              "</tr>");
                                }else if(filetype.equalsIgnoreCase("txt")){
                                     cm.writeFile(port_nm, "IssuerName" + delimiter + "ISIN" + delimiter +  "Sedol" + delimiter + 
                                                  "CABID" + delimiter + "Situation" + delimiter + "DeliveryTimeGMT" + delimiter);
                                }else if(filetype.equalsIgnoreCase("csv")){
                                    cm.writeFile(port_nm, "\"IssuerName\"" + delimiter + "\"ISIN\"" + delimiter +  "\"Sedol\"" + delimiter + 
                                                  "\"CABID\"" + delimiter + "\"Situation\"" + delimiter + "\"DeliveryTimeGMT\"" + delimiter);
                                }
                                
                                
                                while(hrs.next()){
                                    if(isHTML){
                                        cm.writeHTML(port_nm, "<tr bgcolor=white>" +                                                    
                                                 "<td>" + hrs.getString("Issuername") + "</td>" +
                                                 "<td>" + hrs.getString("isin") + "</td>" +
                                                 "<td>" + hrs.getString("sedol") + "</td>" +
                                                 "<td><A HREF='"+url+"/ukcab/full_view.php?" + 
                                                 "cab_id="+hrs.getString("CabEpoch")+"&cab_count="+hrs.getString("CABCount")+"&PSecId="+hrs.getString("PSecId")+
                                                 "&uid="+usr_id+"&cid="+CId+"' target='blank'>" + hrs.getString("CABID") + "/" + hrs.getString("CABCount") + "</a></td>" +
                                                 "<td>" + hrs.getString("Situation") + "</td>" +
                                                 "<td nowrap>" + hrs.getString("DeliveryTimeGMT") + "</td>" +                                                    
                                                 "</tr>");
                                    }else if(filetype.equalsIgnoreCase("txt")){
                                        cm.writeFile(port_nm, hrs.getString("Issuername") + delimiter + hrs.getString("isin") + delimiter +  hrs.getString("sedol") + delimiter + 
                                                  hrs.getString("CabEpoch") + delimiter + hrs.getString("Situation") + delimiter + hrs.getString("DeliveryTimeGMT"));
                                    }else if(filetype.equalsIgnoreCase("csv")){
                                        cm.writeFile(port_nm, 
                                           "\"" + hrs.getString("Issuername") + "\"" + delimiter + "\"" + hrs.getString("ISIN") + "\"" + delimiter +  
                                           "\"" + hrs.getString("sedol") + "\"" + delimiter + "\"" + hrs.getString("CabEpoch") + "\"" + delimiter + 
                                           "\"" + hrs.getString("Situation") + "\"" + delimiter + "\"" + hrs.getString("DeliveryTimeGMT") + "\"");
                                    }
                                    isdata = true;

                                }
                                
                                
                            }
                    hrs.close();
                   if(isHTML){
                        cm.writeHTML(port_nm, "</table>");
                        cm.writeHTML(port_nm, "</td></tr><tr><td><hr></td></tr>");
                   }else if(filetype.equalsIgnoreCase("txt")){
                         cm.writeFile(port_nm, "EDI_ENDOFFILE");                
                   }else if(filetype.equalsIgnoreCase("csv")){
                         cm.writeFile(port_nm, "\"EDI_ENDOFFILE\"");
                   }
                
               
               //CloseConn();
               if(!isdata){
                    
                    if(isHTML){
                        cm.DeleteFile(port_nm);
                        cm.writeHTML(port_nm, "<html lang='en'> " +
                                    "<head> " +
                                    "<title>EDI UK Corporate Actions Bulletin (WinCAB) Email Notification </title> " +
                                    "</head> " +
                                    "<body background-color='#FFFFF8'>  " +
                                    "<table border=0 width=820px align=left bgcolor=white> " +
                                    "<tr><td align=center><P><b>EDI UK Corporate Actions Bulletin (WinCAB) Email Announcement  - " + pname + " - " + pub_dt + "<b></p></td></tr> " +
                                    "<tr><td align=center><P><b>No Record Match Your Portfolio<b></p></td></tr> ");
                    }
               }
               if(isHTML){
                   cm.writeHTML(port_nm, "</table></body></html>");
               }
               return 0;

        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"GetPortfolio: " + e.getMessage());
            }else{
                log.append("(EDI)GetPortfolio: " + e.getMessage(), LogFilePath, "edi_port_emailer");
            }
            cm.DeleteFile(port_nm);
            //CloseConn();
            return -1;
        }

    }

    


    private ResultSet GetukcabPortSummary(String pub_dt,int usr_id, int port_id) {
        try {
            String where_clause = "";
            // Gets the users selected events.
            String sql = "Select count(SitCd) as cnt From portfolio.wol_sit   Where user_id = " + usr_id;
            Statement stwh = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rswh = stwh.executeQuery(sql);
            rswh.next();
            if(rswh.getInt("cnt") > 0){
                where_clause = " And  Cab.SitCD In (Select SitCD From portfolio.wol_sit  Where user_id =" + usr_id + ") ";
            }else{
                //where_clause = " And  Cab.SitCD Not In (Select SitCD From userpreference.wol_sit_alwd  Where user_id =" + usr_id + ") ";
                
            }
            rswh.close();
            stwh.close();
            
            ResultSet Drs = null;
            Statement Dst = conn.createStatement();
 	    String Sql= "Select Distinct Cab.Issuername,Sec.Isin,Sec.Sedol, " +
                        " Cab.DisplayID As CABID , CabTemplate.SitName As Situation, " +
                        " Cab.DisplayDate as DeliveryTimeGMT, Cab.CabEpoch,Cab.CabEdition AS CABCount,Cab.PSecID" +
                        " From wol.Cab" +
                        " left outer Join wol.CabTemplate On wol.Cab.SitCD = wol.CabTemplate.SitCD" +
                        " left outer Join wol.Sec On wol.Cab.CabEpoch=wol.Sec.CabEpoch" +
                        " Where ((Cab.DisplayDate Between '" + pub_dt + "' And '" + pub_dt + " 23:59:59')" +
                        " OR (Cab.Acttime  Between '" + pub_dt + "' And '" + pub_dt + " 23:59:59'))" +
                        " And CABStatus ='SENT' " + where_clause +
                  " And ( " +
                  " sedol IN (Select codes  from portfolio.sedol where  port_id= " + port_id + " and user_id=  " + usr_id + ") OR  " +                  
                  " isin IN (Select codes  from portfolio.isin where port_id= " + port_id + " and user_id= " + usr_id + ")) ";

//            Sql = Sql.substring(0,Sql.length()-7);
	    Sql += " Order by Issuername Asc";
            Drs = Dst.executeQuery(Sql);
            return Drs;
        } catch (Exception e) {
            return null;
        }
    }    


}

