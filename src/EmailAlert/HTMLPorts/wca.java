/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @author h.patel
 */

package EmailAlert.HTMLPorts;

import EmailAlert.lib.CommonMethods;
import com.edi.common.Logging;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import com.edi.common.MySqlConnection.MySqlConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class wca {

    CommonMethods cm = new CommonMethods("",0);
    private Logging log = new Logging("Automation");
    private static Connection conn;
    private String LogFilePath = "";
    private int bAutoRun = 0;
    private String AppPath = System.getProperty("user.dir");
    private ArrayList event = new ArrayList();
    private ArrayList dte_type = new ArrayList();
    private int afrom=0;
    private int ato=0 ;
    private int inc_option=0;
    private String atype="ann";
    private boolean isdata = false;
    private int increm=0;
    private int tfrom=0;
    private int tto=0;
    private String url = "";
    
    public wca(String Log, int Auto,String atyp,int af,int at,int ioption,int incr,int tmfrom, int tmto,Connection conn1, String url){
        cm = new CommonMethods(Log,Auto);
        LogFilePath = Log;
        bAutoRun = Auto;
        //conn = new MySqlConnection(AppPath + "\\edi_port_emailer.cfg", "portfolio").getConnection();
        conn = conn1;
        afrom= af;
        ato= at;
        inc_option = ioption;
        atype = atyp;
        increm= incr;
        tfrom= tmfrom;
        tto= tmto;
        this.url = url;
    }

    public int GetPortfolio(int usr_id, int port_id, String pub_dt, String port_nm, String pname, String Codes,int CId, String filetype, String delimiter){
        try{
            
            boolean isHTML = true; 
            if(filetype.equalsIgnoreCase("txt")){
                isHTML = false;
                if(delimiter.contains("\\t")){
                    delimiter = "\t";
                }                
            }else if(filetype.equalsIgnoreCase("csv")){
                isHTML = false;
                delimiter = ",";                
            }
           
            String type = "Announcement";
            
            if(atype.equalsIgnoreCase("alr")) type = "Alert";
            cm.DeleteFile(port_nm);
            String header = pub_dt;
            if(increm!=0){
                header = pub_dt.substring(0,10) + " Incremental: " + increm;
            }
            if(isHTML){
                cm.writeHTML(port_nm, "<html lang='en'> " +
                            "<head> " +
                            "<title>EDI Worldwide Corporate Actions Email " + type + " </title> " +
                            "</head> " +
                            "<body background-color='#FFFFF8'>  " +
                            "<table border=0 width=99% top=99% align=left bgcolor=white> " +
                            "<tr><td align=center><P><br>EDI Worldwide Corporate Actions Email " + type + " - " + pname + " - " + header + "<b></p></td></tr> " +
                            "<tr><td align=center><P><b>Portfolio Summary<b></p></td></tr> " +
                            "<tr> " +
                            "<td nowrap> ");
            }else if(filetype.equalsIgnoreCase("txt")){
                 cm.writeFile(port_nm, "EDI Worldwide Corporate Actions Email " + type + " - " + pname + " - " + header);                
            }else if(filetype.equalsIgnoreCase("csv")){
                 cm.writeFile(port_nm, "\"EDI Worldwide Corporate Actions Email " + type + " - " + pname + " - " + header + "\"");
            }
                //Get Summary
                if(atype.equalsIgnoreCase("alr"))
                    GetEventALR(usr_id);
                else
                    GetEvent(usr_id);
                if(event.size() > 0){
                    if(isHTML){
                        cm.writeHTML(port_nm, "<table border=0 width=99% top=99% align=left bgcolor='#E8E8E8'><tr bgcolor=darkGray>" +
                                          "<td><b>EventID</b></td>" +
                                          "<td><b>Event</b></td>" +
                                          "<td><b>InCorp</b></td>" +
                                          "<td><b>IssueName</b></td>" +
                                          "<td><b>ISIN</b></td>" +
                                          "<td><b>Sedol</b></td>" +
                                          "<td><b>UsCode</b></td>" +
                                          "<td><b>Localcode</b></td>" +
                                          "<td><b>Exchange</b></td>" +
                                          "<td><b>Issue</b></td>" +
                                          "</tr>");
                    }else if(filetype.equalsIgnoreCase("txt")){
                         cm.writeFile(port_nm, "EventID" + delimiter + "Event" + delimiter +  "InCorp" + delimiter + 
                                      "IssueName" + delimiter + "ISIN" + delimiter + "Sedol" + delimiter + "UsCode" + delimiter
                                      + "Localcode" + delimiter  + "Exchange" + delimiter  + "Issue");                
                    }else if(filetype.equalsIgnoreCase("csv")){
                         cm.writeFile(port_nm, 
                                     "\"EventID\"" + delimiter + "\"Event\"" + delimiter +  "\"InCorp\"" + delimiter + 
                                      "\"IssueName\"" + delimiter + "\"ISIN\"" + delimiter + "\"Sedol\"" + delimiter
                                       + "\"UsCode\"" + delimiter  + "\"Localcode\"" + delimiter + "\"Exchange\"" + delimiter
                                       + "\"Issue\"");
                    }
                    
                    for(int z=0;z<event.size();z++){
                            ResultSet hrs = null;
                            if(atype.equalsIgnoreCase("alr"))
                                hrs = GetWCAPortSummaryALR(event.get(z).toString(), pub_dt, usr_id, port_id, dte_type.get(z).toString());
                            else
                                hrs = GetWCAPortSummary(event.get(z).toString(), pub_dt, usr_id, port_id);
                            if(hrs != null){
                                    while(hrs.next()){
                                        System.out.println(hrs.getRow());
                                            if(isHTML){
                                                cm.writeHTML(port_nm, "<tr bgcolor=white>" +
                                                        "<td><A HREF='"+url+"/wca/full_view.php"+
                                                        "?Caref="+hrs.getString("caref")+"&sedol="+hrs.getString("sedol")+"&event="+hrs.getString("event").replaceAll(" ", "_")+"&eventid="+hrs.getString("eventid")+"&secid="+hrs.getString("secid")+"&exchgcd="+hrs.getString("exchange_code")+
                                                        "&uid="+usr_id+"&cid="+CId+"' target='blank'>" + hrs.getString("eventid") + "</a></td>" +
                                                         "<td>" + capitalize(hrs.getString("event")) + "</td>" +
                                                         "<td>" + hrs.getString("country_of_incorporation") + "</td>" +
                                                         "<td>" + hrs.getString("issuer_name") + "</td>" +
                                                         "<td>" + hrs.getString("isin") + "</td>" +
                                                         "<td>" + hrs.getString("sedol") + "</td>" +
                                                         "<td>" + hrs.getString("us_code") + "</td>" +
                                                         "<td>" + hrs.getString("local_code") + "</td>" +
                                                         "<td>" + hrs.getString("exchange_code") + "</td>" +
                                                         "<td>" + hrs.getString("security_description") + "</td>" +
                                                         "</tr>");
                                            }else if(filetype.equalsIgnoreCase("txt")){
                                                cm.writeFile(port_nm, hrs.getString("eventid") + delimiter + capitalize(hrs.getString("event")) + delimiter +  hrs.getString("country_of_incorporation") + delimiter + 
                                                            hrs.getString("issuer_name") + delimiter + hrs.getString("isin") + delimiter + hrs.getString("sedol") + delimiter + hrs.getString("us_code")+ delimiter + 
                                                            hrs.getString("local_code") + delimiter + hrs.getString("exchange_code") + delimiter + hrs.getString("security_description"));
                                            }else if(filetype.equalsIgnoreCase("csv")){
                                                cm.writeFile(port_nm, 
                                                              "\"" + hrs.getString("eventid") + "\"" + delimiter + "\"" + capitalize(hrs.getString("event")) + "\"" + delimiter +  "\"" + hrs.getString("country_of_incorporation") + "\"" + delimiter + 
                                                              "\"" + hrs.getString("issuer_name") + "\"" + delimiter + "\"" + hrs.getString("isin") + "\"" + delimiter + "\"" + hrs.getString("sedol") + "\"" + delimiter + 
                                                              "\"" + hrs.getString("us_code") + "\"" + delimiter + "\"" + hrs.getString("local_code") + "\"" + delimiter + "\"" + hrs.getString("exchange_code") + "\"" + delimiter + 
                                                              "\"" + hrs.getString("security_description") + "\"");
                                            }
                                            isdata = true;

                                    }
                                    hrs.close();
                            }
                        
                    }
                    if(isHTML){
                        cm.writeHTML(port_nm, "</table>");
                        cm.writeHTML(port_nm, "</td></tr><tr><td><hr></td></tr>");
                    }else if(filetype.equalsIgnoreCase("txt")){
                         cm.writeFile(port_nm, "EDI_ENDOFFILE");                
                    }else if(filetype.equalsIgnoreCase("csv")){
                         cm.writeFile(port_nm, "\"EDI_ENDOFFILE\"");
                    }
                }
               
               //CloseConn();
               if(!isdata){
                    
                    if(isHTML){
                        cm.DeleteFile(port_nm);
                        cm.writeHTML(port_nm, "<html lang='en'> " +
                                    "<head> " +
                                    "<title>EDI Worldwide Corporate Actions Email " + type + " </title> " +
                                    "</head> " +
                                    "<body background-color='#FFFFF8'>  " +
                                    "<table border=0 width=99% top=99% align=left bgcolor=white> " +
                                    "<tr><td align=center><P><br>EDI Worldwide Corporate Actions Email " + type + " - " + pname + " - " + pub_dt + "<b></p></td></tr> " +
                                    "<tr><td align=center><P><b>No Record Match Your Portfolio<b></p></td></tr> ");
                    }
               }
               if(isHTML){cm.writeHTML(port_nm, "</table></body></html>");}
               return 0;

        }catch(Exception e){
            if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"GetPortfolio: " + e.getMessage());
            }else{
                log.append("(EDI)GetPortfolio: " + e.getMessage(), LogFilePath, "edi_port_emailer");
            }
            cm.DeleteFile(port_nm);
            //CloseConn();
            return -1;
        }
    }

     private void GetEvent(int usr_id) {
        try {
            ResultSet Srs = null;
            Statement Sst = conn.createStatement();
            String sql="";
            sql = "Select Event from portfolio.wca_event  Where user_id="+ usr_id +" and type='ann' Order by Event Asc";
            Srs = Sst.executeQuery(sql);
	    if(Srs !=null){
		while(Srs.next()){
			event.add(Srs.getString("Event"));
		}
	    }

            if(event.isEmpty()){
                sql = " Select Event from prdxtra.wca_event_lookup  Where  Event Not IN (select Event from userpreference.wca_Event_blck where user_id = "+ usr_id +") and Event like 'evf_%' Order by Event Asc";
                Srs = Sst.executeQuery(sql);
                if(Srs !=null){
                    while(Srs.next()){
                            event.add(Srs.getString("Event"));
                    }
                }
            }
	    Srs.close();
	    Sst.close();
        } catch (Exception e) {
	    if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"GetEvent: " + e.getMessage());
            }else{
                log.append("(EDI)GetEvent: " + e.getMessage(), LogFilePath, "edi_port_emailer");
            }
        }
    }

    private void GetEventALR(int usr_id) {
        try {
            ResultSet Srs = null;
            Statement Sst = conn.createStatement();
            String sql="";
            sql = "Select Event, dte_type from portfolio.wca_event  Where user_id="+ usr_id +" and type='alr' Order by Event Asc";
            Srs = Sst.executeQuery(sql);
	    if(Srs !=null){
		while(Srs.next()){
			event.add(Srs.getString("Event"));
                        dte_type.add(Srs.getString("dte_type"));                      
		}
	    }
	    Srs.close();
	    Sst.close();
        } catch (Exception e) {
	    if (bAutoRun==0 || bAutoRun==1){
                javax.swing.JOptionPane.showMessageDialog(null,"GetEvent: " + e.getMessage());
            }else{
                log.append("(EDI)GetEvent: " + e.getMessage(), LogFilePath, "edi_port_emailer");
            }
        }
    }

    private ResultSet GetWCAPortSummary(String event,String pub_dt,int usr_id, int port_id) {
        try {
            ResultSet Drs = null;
            Statement Dst = conn.createStatement();
 	    
            String Sql= " Select caref, eventid, event, country_of_incorporation, Issuer_Name, isin, sedol, us_code, local_code, exchange_code, security_description, secid From wca2." + event.toLowerCase();
            //Towfik Added for increamental option 
            
            if (tfrom!=0 && tto!=0){                
                Sql+= " Where (changed BETWEEN '" + pub_dt + " " + tfrom + ":00:00' AND '" + pub_dt +" "+ tto + ":00:00' "+
                          " OR created BETWEEN '" + pub_dt + " " + tfrom + ":00:00' AND '" + pub_dt +" "+ tto + ":00:00') ";
            }else if(increm==0){
                Sql+= " Where (changed BETWEEN '" + pub_dt + "' AND '" + pub_dt + " 23:59:59' "+
                        " OR   created BETWEEN '" + pub_dt + "' AND '" + pub_dt + " 23:59:59') ";
            }else{
                Sql+= " Where (changed >= '" + pub_dt + "' "+
                      " OR created >= '" + pub_dt + "' )";  
            }
            Sql+= " And ( " +
                  " us_code IN (Select codes  from portfolio.uscode where port_id= " + port_id + " and user_id= " + usr_id + ") OR  " +
                  " sedol IN (Select codes  from portfolio.sedol where  port_id= " + port_id + " and user_id=  " + usr_id + ") OR  " +
                  " isin IN (Select codes  from portfolio.isin where  port_id= " + port_id + " and user_id= " + usr_id + ") OR "+
                  " bloomberg_composite_ticker IN (Select codes  from portfolio.bloomberg_composite_ticker where  port_id= " + port_id + " and user_id= " + usr_id + ") OR "+
                  " bloomberg_exchange_ticker IN (Select codes  from portfolio.bloomberg_exchange_ticker where  port_id= " + port_id + " and user_id= " + usr_id + ")) ";

            if (inc_option==0){Sql += " And primary_exchange = 'Yes' ";}
            Sql += " Order by issuer_name asc ,eventid Desc";
              System.out.println(Sql);
            Drs = Dst.executeQuery(Sql);
            return Drs;
        } catch (Exception e) {
            return null;
        }
    }

    private ResultSet GetWCAPortSummaryALR(String event,String pub_dt,int usr_id, int port_id, String dte_type) {
         String Sql="";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();

            //Get pub_dt1
            c.setTime(sdf.parse(pub_dt));
            c.add(Calendar.DATE, afrom);  
            String pub_dt1 = sdf.format(c.getTime());

            //Get pub_dt2
            c.setTime(sdf.parse(pub_dt));
            c.add(Calendar.DATE, ato);  
            String pub_dt2 = sdf.format(c.getTime());
            
            //Create WHERE Clause for date type
            String [] dte = dte_type.split(",");
            String Wclause = "";
            for(int z=0;z<dte.length;z++){
                Wclause += "("+ dte[0]  + " BETWEEN '" + pub_dt1 + "' AND '" + pub_dt2 + " 23:59:59') OR ";
            }
            Wclause = Wclause.substring(0, Wclause.length() - 3);

            ResultSet Drs = null;
            Statement Dst = conn.createStatement();
 	    Sql= " Select caref, eventid, event, country_of_incorporation, Issuer_Name, isin, sedol, us_code, local_code, exchange_code, security_description, secid From wca2." + event +
                  " Where ( " + Wclause + " )";
            Sql+= " And ( " +
                  " us_code IN (Select codes  from portfolio.uscode where  port_id= " + port_id + " and user_id= " + usr_id + ") OR  " +
                  " sedol IN (Select codes  from portfolio.sedol where  port_id= " + port_id + " and user_id=  " + usr_id + ") OR  " +
                  " isin IN (Select codes  from portfolio.isin where  port_id= " + port_id + " and user_id= " + usr_id + ")) ";
            if (inc_option==0){Sql += " And primary_exchange = 'Yes' ";}
            Sql += " Order by issuer_name asc ,eventid Desc";
            System.out.println(Sql);
            Drs = Dst.executeQuery(Sql);
            return Drs;
        } catch (Exception e) {
            return null;
        }
    }

    private String capitalize(String s){
        try{
            String [] letter = null;
            if(s.contains("_"))letter = s.split("_");
            else letter = s.split(" ");
            s = "";
            for(int f=0;f<letter.length;f++){
                s += letter[f].substring(0,1).toUpperCase() + letter[f].substring(1) + " ";
            }
            s = s.substring(0,s.length()-1);
            return s;
        }catch(Exception e){
            return "";
        }
    }
    //Towfik Added for increamental option 
    
}
